package me.tonyhhyip.jvm.validation;

import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.ElementType.CONSTRUCTOR;
import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.PARAMETER;
import static java.lang.annotation.ElementType.TYPE_USE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;
import java.util.Arrays;
import java.util.List;
import javax.validation.Constraint;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import me.tonyhhyip.jvm.validation.OneOf.Validator;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/** */
@Target({TYPE_USE, FIELD, METHOD, CONSTRUCTOR, PARAMETER, ANNOTATION_TYPE})
@Retention(RUNTIME)
@Documented
@Constraint(validatedBy = {Validator.class})
public @interface OneOf {
  String message() default "{me.tonyhhyip.jvm.validation.OneOf.message}";

  String[] value();

  Class<?>[] groups() default {};
  Class<?>[] payload() default {};

  class Validator implements ConstraintValidator<OneOf, String> {
    private List<String> allowedValues;

    @Override
    public void initialize(@NotNull OneOf annotation) {
      allowedValues = Arrays.asList(annotation.value());
    }

    @Override
    public boolean isValid(@Nullable String value, @Nullable ConstraintValidatorContext context) {
      if (value == null) {
        return true;
      }
      return allowedValues.contains(value);
    }
  }
}
