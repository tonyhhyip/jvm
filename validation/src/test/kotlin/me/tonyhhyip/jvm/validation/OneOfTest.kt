package me.tonyhhyip.jvm.validation

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import javax.validation.Validation

class OneOfTest {
    private data class TestData(@OneOf(value = ["a", "b"]) val str: String)

    @Test
    fun testIsValid() {
        val validator = Validation.buildDefaultValidatorFactory().validator
        Assertions.assertEquals(0, validator.validate(TestData("a")).size)
        Assertions.assertEquals(1, validator.validate(TestData("c")).size)
    }
}
