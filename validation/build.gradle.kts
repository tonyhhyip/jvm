plugins {
    jacoco
    kotlin("jvm")
    `maven-publish`
}

val jacocoVersion: String by rootProject.extra
val googleJavaVersion: String by rootProject.extra
val ktlintVersion: String by rootProject.extra

spotless {
    kotlin {
        ktlint(ktlintVersion)
    }
    java {
        googleJavaFormat(googleJavaVersion)
    }
}

jacoco {
    toolVersion = jacocoVersion
}

tasks.jacocoTestReport {
    reports {
        xml.required.set(true)
        xml.outputLocation.set(file("$buildDir/jacoco/jacoco.xml"))
        html.outputLocation.set(file("$buildDir/jacoco/html"))
    }
}

dependencies {
    implementation("jakarta.validation:jakarta.validation-api:2.0.2")
    testImplementation(platform("org.junit:junit-bom:5.7.2"))
    testImplementation("org.junit.jupiter:junit-jupiter")
    testImplementation(kotlin("stdlib-jdk8"))
    testImplementation(kotlin("reflect"))
    testImplementation("org.hibernate.validator:hibernate-validator:6.2.0.Final")
    testImplementation("org.glassfish:javax.el:3.0.0")
}

publishing {
    repositories {
        maven {
            val gitlabToken: String? by project
            name = "gitlab"
            url = uri("https://gitlab.com/api/v4/projects/22919357/packages/maven")
            credentials(HttpHeaderCredentials::class) {
                name = "Private-Token"
                value = gitlabToken
            }
            authentication {
                create<HttpHeaderAuthentication>("header")
            }
        }
    }
    publications {
        create<MavenPublication>("maven") {
            groupId = "me.tonyhhyip.jvm.validation"
            artifactId = "validation"
            version = "1.0.0"

            from(components["java"])
            pom {
                name.set("Extra Validation annotation")
                licenses {
                    license {
                        name.set("GNU Lesser General Public License version 3")
                        url.set("https://opensource.org/licenses/LGPL-3.0")
                    }
                }
                developers {
                    developer {
                        id.set("tonyhhyip")
                        name.set("Tony Yip")
                        email.set("tony@tonyhhyip.me")
                    }
                }
            }
        }
    }
}
