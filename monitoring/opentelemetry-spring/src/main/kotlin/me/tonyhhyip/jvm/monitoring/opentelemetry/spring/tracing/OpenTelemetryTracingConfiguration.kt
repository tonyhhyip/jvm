package me.tonyhhyip.jvm.monitoring.opentelemetry.spring.tracing

import io.opentelemetry.api.baggage.propagation.W3CBaggagePropagator
import io.opentelemetry.api.common.AttributeKey
import io.opentelemetry.api.common.Attributes
import io.opentelemetry.api.trace.Tracer
import io.opentelemetry.context.propagation.TextMapPropagator
import io.opentelemetry.extension.annotations.WithSpan
import io.opentelemetry.sdk.OpenTelemetrySdk
import io.opentelemetry.sdk.resources.Resource
import io.opentelemetry.sdk.trace.SdkTracerProvider
import io.opentelemetry.sdk.trace.export.SimpleSpanProcessor
import io.opentelemetry.sdk.trace.export.SpanExporter
import io.opentelemetry.sdk.trace.samplers.Sampler
import me.tonyhhyip.jvm.monitoring.opentelemetry.spring.OpenTelemetryConfigurationProperties
import me.tonyhhyip.jvm.monitoring.opentelemetry.spring.tracing.exporter.OpenTelemetrySpanExporterConfiguration
import org.aspectj.lang.annotation.Aspect
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Import

@Configuration(proxyBeanMethods = false)
@Import(OpenTelemetrySpanExporterConfiguration::class)
class OpenTelemetryTracingConfiguration {
    @ConditionalOnClass(Aspect::class, WithSpan::class)
    fun withSpanAspect(tracer: Tracer) = WithSpanAspect(tracer)

    @ConditionalOnWebApplication
    @Bean
    fun tracingFilter(tracer: Tracer, textMapPropagator: TextMapPropagator) = TracingFilter(tracer, textMapPropagator)

    @Bean
    fun tracer(sdk: OpenTelemetrySdk): Tracer {
        return sdk.getTracer("me.tonyhhyip.jvm.monitoring.opentelemetry.spring")
    }

    @Bean(destroyMethod = "close")
    fun tracerProvider(
        resource: Resource,
        exporters: List<SpanExporter>,
        sampler: Sampler,
    ): SdkTracerProvider {
        return SdkTracerProvider.builder()
            .addSpanProcessor(SimpleSpanProcessor.create(SpanExporter.composite(exporters)))
            .setResource(resource)
            .setSampler(sampler)
            .build()
    }

    @Bean
    fun otelResource(properties: OpenTelemetryConfigurationProperties): Resource {
        if (properties.serviceName == null) {
            return Resource.getDefault()
        }
        return Resource.getDefault()
            .merge(
                Resource.create(
                    Attributes.of(AttributeKey.stringKey("service.name"), properties.serviceName)
                )
            )
    }

    @Bean
    fun sampler(properties: OpenTelemetryConfigurationProperties): Sampler {
        if (properties.sample == null) {
            return Sampler.traceIdRatioBased(0.1)
        }
        return Sampler.traceIdRatioBased(properties.sample.toDouble() / 100)
    }

    @ConditionalOnMissingBean(TextMapPropagator::class)
    @Bean
    fun textMapPropagator(): TextMapPropagator = W3CBaggagePropagator.getInstance()
}
