package me.tonyhhyip.jvm.monitoring.opentelemetry.spring.tracing

import io.opentelemetry.api.common.AttributeKey
import io.opentelemetry.api.common.Attributes
import io.opentelemetry.api.trace.SpanKind
import io.opentelemetry.api.trace.Tracer
import io.opentelemetry.context.Context
import io.opentelemetry.context.propagation.TextMapGetter
import io.opentelemetry.context.propagation.TextMapPropagator
import org.springframework.core.Ordered
import org.springframework.web.filter.OncePerRequestFilter
import javax.servlet.FilterChain
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

class TracingFilter(
    private val tracer: Tracer,
    private val textMapPropagator: TextMapPropagator,
) : OncePerRequestFilter(), Ordered {
    private val getter = object : TextMapGetter<HttpServletRequest> {
        override fun get(carrier: HttpServletRequest?, key: String): String? {
            return carrier?.let { carrier.getHeader(key) }
        }

        override fun keys(carrier: HttpServletRequest): Iterable<String> {
            return carrier.headerNames.toList()
        }
    }

    override fun getOrder(): Int {
        return Ordered.HIGHEST_PRECEDENCE + 1
    }

    override fun doFilterInternal(
        request: HttpServletRequest,
        response: HttpServletResponse,
        filterChain: FilterChain
    ) {
        textMapPropagator.extract(Context.current(), request, getter)
            .makeCurrent().use {
                val span = tracer.spanBuilder("${request.method} ${request.requestURI}")
                    .setSpanKind(SpanKind.SERVER)
                    .startSpan().also {
                        it.setAllAttributes(
                            Attributes.of(
                                AttributeKey.stringKey("http.method"), request.method,
                                AttributeKey.stringKey("http.scheme"), request.scheme,
                                AttributeKey.stringKey("http.host"), request.remoteHost,
                                AttributeKey.stringKey("http.target"), request.requestURI,
                            )
                        )
                    }
                try {
                    span.makeCurrent().use {
                        filterChain.doFilter(request, response)
                    }
                } finally {
                    span.end()
                }
            }
    }
}
