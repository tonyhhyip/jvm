package me.tonyhhyip.jvm.monitoring.opentelemetry.spring.tracing

import io.opentelemetry.api.trace.Tracer
import io.opentelemetry.extension.annotations.WithSpan
import org.aspectj.lang.ProceedingJoinPoint
import org.aspectj.lang.annotation.Around
import org.aspectj.lang.annotation.Aspect
import org.aspectj.lang.reflect.MethodSignature
import java.lang.reflect.Method

@Aspect
class WithSpanAspect(private val tracer: Tracer) {
    @Around("@annotation(io.opentelemetry.extension.annotations.WithSpan)")
    @Throws(Throwable::class)
    fun traceMethod(pjp: ProceedingJoinPoint): Any? {
        val signature = pjp.signature as MethodSignature
        val method = signature.method
        val withSpan = method.getAnnotation(WithSpan::class.java)
        val span = tracer.spanBuilder(spanName(withSpan, method))
            .setSpanKind(withSpan.kind)
            .startSpan()
        try {
            span.makeCurrent().use { return pjp.proceed() }
        } finally {
            span.end()
        }
    }

    companion object {
        @JvmStatic private fun spanName(annotation: WithSpan, method: Method): String {
            val spanName = annotation.value
            if (spanName.isNotEmpty()) {
                return spanName
            }
            return spanNameForMethod(method)
        }

        @JvmStatic private fun spanNameForMethod(method: Method): String {
            return "${spanNameForClass(method.declaringClass)}.${method.name}"
        }

        @JvmStatic private fun spanNameForClass(clazz: Class<*>): String {
            if (!clazz.isAnonymousClass) {
                return clazz.simpleName
            }
            val className = clazz.name
            if (clazz.`package` != null) {
                val pkgName = clazz.`package`.name
                if (pkgName.isNotEmpty()) {
                    return className.substring(pkgName.length + 1)
                }
            }
            return className
        }
    }
}
