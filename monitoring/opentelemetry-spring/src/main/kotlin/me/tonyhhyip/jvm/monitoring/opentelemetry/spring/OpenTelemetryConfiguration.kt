package me.tonyhhyip.jvm.monitoring.opentelemetry.spring

import io.opentelemetry.context.propagation.ContextPropagators
import io.opentelemetry.context.propagation.TextMapPropagator
import io.opentelemetry.sdk.OpenTelemetrySdk
import io.opentelemetry.sdk.trace.SdkTracerProvider
import me.tonyhhyip.jvm.monitoring.opentelemetry.spring.tracing.OpenTelemetryTracingConfiguration
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Import

@Configuration
@EnableConfigurationProperties(OpenTelemetryConfigurationProperties::class)
@Import(OpenTelemetryTracingConfiguration::class)
class OpenTelemetryConfiguration {
    @Bean
    fun openTelemetrySdk(
        tracerProvider: SdkTracerProvider,
        @Autowired(required = false) textMapPropagator: TextMapPropagator
    ): OpenTelemetrySdk {
        return OpenTelemetrySdk.builder()
            .setTracerProvider(tracerProvider)
            .setPropagators(ContextPropagators.create(textMapPropagator))
            .build()
    }
}
