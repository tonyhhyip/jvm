package me.tonyhhyip.jvm.monitoring.opentelemetry.spring

import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Import

@Configuration
@Target(AnnotationTarget.TYPE, AnnotationTarget.CLASS)
@Retention(AnnotationRetention.RUNTIME)
@Import(OpenTelemetryConfiguration::class)
annotation class EnableOpenTelemetry
