package me.tonyhhyip.jvm.monitoring.opentelemetry.spring.tracing.exporter

import io.opentelemetry.exporter.zipkin.ZipkinSpanExporter
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.ConstructorBinding
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import java.time.Duration

@ConditionalOnClass(ZipkinSpanExporter::class)
@ConditionalOnProperty("monitoring.otel.tracing.zipkin.enabled", havingValue = "true")
@Configuration(proxyBeanMethods = false)
@EnableConfigurationProperties(
    OpenTelemetryZipkinExporterConfiguration.OpenTelemetryZipkinExporterConfigurationProperties::class,
)
class OpenTelemetryZipkinExporterConfiguration {
    @ConfigurationProperties(prefix = "monitoring.otel.tracing.zipkin")
    @ConstructorBinding
    data class OpenTelemetryZipkinExporterConfigurationProperties(
        val enabled: Boolean? = false,
        val endpoint: String?,
        val timeout: Long?,
    )

    @Bean(destroyMethod = "shutdown")
    fun zipkinSpanExporter(properties: OpenTelemetryZipkinExporterConfigurationProperties): ZipkinSpanExporter {
        return ZipkinSpanExporter.builder()
            .also {
                if (properties.endpoint != null) {
                    it.setEndpoint(properties.endpoint)
                }
                if (properties.timeout != null) {
                    it.setReadTimeout(Duration.ofSeconds(properties.timeout))
                }
            }
            .build()
    }
}
