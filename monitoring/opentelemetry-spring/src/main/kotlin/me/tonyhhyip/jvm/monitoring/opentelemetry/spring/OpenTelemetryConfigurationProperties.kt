package me.tonyhhyip.jvm.monitoring.opentelemetry.spring

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.ConstructorBinding

@ConfigurationProperties(prefix = "monitoring.otel")
@ConstructorBinding
data class OpenTelemetryConfigurationProperties(
    val serviceName: String?,
    val sample: Int?,
)
