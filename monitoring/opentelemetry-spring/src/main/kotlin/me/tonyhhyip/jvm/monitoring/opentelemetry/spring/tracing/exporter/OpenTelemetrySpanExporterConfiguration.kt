package me.tonyhhyip.jvm.monitoring.opentelemetry.spring.tracing.exporter

import io.opentelemetry.exporter.logging.LoggingSpanExporter
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Import

@Configuration
@Import(
    OpenTelemetryJaegerExporterConfiguration::class,
    OpenTelemetryZipkinExporterConfiguration::class,
)
class OpenTelemetrySpanExporterConfiguration {
    @ConditionalOnClass(LoggingSpanExporter::class)
    @ConditionalOnProperty("monitoring.otel.tracing.exporter.logging.enabled", havingValue = "true")
    @Bean(destroyMethod = "shutdown")
    fun loggingExporter() = LoggingSpanExporter()
}
