package me.tonyhhyip.jvm.monitoring.opentelemetry.spring.tracing.exporter

import io.opentelemetry.exporter.jaeger.JaegerGrpcSpanExporter
import io.opentelemetry.exporter.jaeger.thrift.JaegerThriftSpanExporter
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.ConstructorBinding
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import java.time.Duration

@ConditionalOnProperty("monitoring.otel.tracing.exporter.jaeger.enabled", havingValue = "true")
@Configuration(proxyBeanMethods = false)
@EnableConfigurationProperties(
    OpenTelemetryJaegerExporterConfiguration.OpenTelemetryJaegerExporterConfigurationProperties::class,
)
class OpenTelemetryJaegerExporterConfiguration {
    @ConfigurationProperties(prefix = "monitoring.otel.tracing.exporter.jaeger")
    @ConstructorBinding
    data class OpenTelemetryJaegerExporterConfigurationProperties(
        val enabled: Boolean? = false,
        val protocol: String = "grpc",
        val endpoint: String?,
        val timeout: Long?,
    )

    @ConditionalOnClass(JaegerGrpcSpanExporter::class)
    @ConditionalOnProperty("monitoring.otel.tracing.exporter.jaeger.protocol", havingValue = "grpc", matchIfMissing = true)
    @Bean(destroyMethod = "shutdown")
    fun jaegerGrpcExporter(properties: OpenTelemetryJaegerExporterConfigurationProperties): JaegerGrpcSpanExporter {
        return JaegerGrpcSpanExporter.builder()
            .setEndpoint(properties.endpoint!!)
            .setTimeout(Duration.ofSeconds(properties.timeout ?: 30))
            .build()
    }

    @ConditionalOnClass(JaegerThriftSpanExporter::class)
    @ConditionalOnProperty("monitoring.otel.tracing.exporter.jaeger.protocol", havingValue = "thrift")
    @Bean(destroyMethod = "shutdown")
    fun jaegerThriftExporter(properties: OpenTelemetryJaegerExporterConfigurationProperties): JaegerThriftSpanExporter {
        return JaegerThriftSpanExporter.builder()
            .setEndpoint(properties.endpoint!!)
            .build()
    }
}
