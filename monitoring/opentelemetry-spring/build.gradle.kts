plugins {
    jacoco
    kotlin("jvm")
    kotlin("plugin.spring")
    kotlin("kapt")

    id("org.springframework.boot")
    id("io.spring.dependency-management")
}

dependencyManagement {
    imports {
        mavenBom("io.opentelemetry:opentelemetry-bom:1.3.0")
    }
}

dependencies {
    api(platform("io.opentelemetry:opentelemetry-bom:1.3.0"))
    api("io.opentelemetry:opentelemetry-api")
    implementation("io.opentelemetry:opentelemetry-sdk")
    api("io.opentelemetry:opentelemetry-extension-kotlin")
    compileOnly("io.opentelemetry:opentelemetry-extension-annotations")
    compileOnly("io.opentelemetry:opentelemetry-exporter-logging")
    compileOnly("io.opentelemetry:opentelemetry-exporter-jaeger")
    compileOnly("io.opentelemetry:opentelemetry-exporter-jaeger-thrift")
    compileOnly("io.opentelemetry:opentelemetry-exporter-zipkin")

    implementation("org.springframework.boot:spring-boot-starter")
    compileOnly("org.springframework.boot:spring-boot-starter-web")
    compileOnly("org.springframework.boot:spring-boot-starter-aop")

    testImplementation("org.junit.jupiter:junit-jupiter")
    testImplementation("org.mockito:mockito-core")

    kapt("org.springframework.boot:spring-boot-configuration-processor")
}

val ktlintVersion: String by rootProject.extra
val jacocoVersion: String by rootProject.extra

spotless {
    kotlin {
        ktlint(ktlintVersion)
    }
}

jacoco {
    toolVersion = jacocoVersion
}

tasks.jacocoTestReport {
    reports {
        xml.required.set(true)
        xml.outputLocation.set(file("$buildDir/jacoco/jacoco.xml"))
        html.outputLocation.set(file("$buildDir/jacoco/html"))
    }
}

tasks.getByName<org.springframework.boot.gradle.tasks.bundling.BootJar>("bootJar") {
    enabled = false
}

tasks.getByName<Jar>("jar") {
    enabled = true
}
