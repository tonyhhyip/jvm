group = "me.tonyhhyip.jvm.framework.guice"

plugins {
    kotlin("jvm")
}

dependencies {
    api("com.google.inject:guice:4.2.3")
    implementation(kotlin("stdlib-jdk8"))
    implementation(kotlin("reflect"))
}
