package me.tonyhhyip.jvm.framerowk.guice

import com.google.inject.Injector
import com.google.inject.Module
import kotlin.reflect.full.primaryConstructor

fun Injector.extend(vararg modules: Module): Injector {
    val additionalModules = mutableListOf<Module>()
    modules.forEach { processModule(additionalModules, it) }
    return this.createChildInjector(additionalModules)
}

internal fun processModule(modules: MutableList<Module>, module: Module) {
    modules.add(module)
    val importModule = module.javaClass.getAnnotation(Import::class.java)
    importModule.classes.forEach {
        val constructor = it.primaryConstructor
        require(constructor != null && constructor.parameters.isEmpty()) { "Module constructor must be empty" }
        processModule(modules, constructor.call())
    }
}
