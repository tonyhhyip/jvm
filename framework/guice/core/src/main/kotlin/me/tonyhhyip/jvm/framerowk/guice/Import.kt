package me.tonyhhyip.jvm.framerowk.guice

import com.google.inject.Module
import kotlin.reflect.KClass

@Target(AnnotationTarget.ANNOTATION_CLASS, AnnotationTarget.CLASS)
@Retention(AnnotationRetention.RUNTIME)
@MustBeDocumented
annotation class Import(val classes: Array<KClass<out Module>>)
