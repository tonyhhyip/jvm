package me.tonyhhyip.jvm.framerowk.guice

import com.google.inject.AbstractModule
import com.google.inject.Guice
import com.google.inject.Injector
import com.google.inject.Module
import com.google.inject.Stage
import java.util.ServiceLoader

object AutoInjectorBuilder {
    private val loader = ServiceLoader.load(Module::class.java)
    private val abstractLoader = ServiceLoader.load(AbstractModule::class.java)

    fun create(): Injector {
        loader.reload()
        abstractLoader.reload()
        val modules = mutableListOf<Module>()
        loader.iterator().forEach { processModule(modules, it) }
        abstractLoader.iterator().forEach { processModule(modules, it) }
        return Guice.createInjector(Stage.PRODUCTION, modules)
    }
}
