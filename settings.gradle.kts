rootProject.name = "jvm"
include(
    "framework:guice:core",
    "bibliography:marc-core", "bibliography:marc-serialize-api",
    "bibliography:marc-serialize-xml-jaxb", "bibliography:marc-serialize-mrk8"
)
include("validation")
include("monitoring:opentelemetry-spring")
