package me.tonyhhyip.bibliography.marc.core;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class LeaderTest {
  @Test
  void testParsing() {
    Leader leader = new Leader("00714cam a2100205 a 4500");
    Assertions.assertEquals("00714cam a2100205 a 4500", leader.toString());
    Assertions.assertEquals(1, leader.getSubfieldCodeCount());
  }
}
