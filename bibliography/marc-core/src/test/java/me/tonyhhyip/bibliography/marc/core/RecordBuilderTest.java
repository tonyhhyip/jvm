package me.tonyhhyip.bibliography.marc.core;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class RecordBuilderTest {
  @Test
  void testBuild() {
    Record record =
        Record.builder()
            .withLeader(Leader.build("00714cam a2100205 a 4500"))
            .addField(ControlField.builder().withTag("001").withData("Testing").build())
            .addField(
                DataField.builder()
                    .withTag("245")
                    .withIndicator1('1')
                    .withIndicator2('0')
                    .addSubfield(
                        SubField.builder().withCode('a').withData("Testing Book Name").build())
                    .build())
            .build();
    Assertions.assertEquals("00714cam a2100205 a 4500", record.getLeader().toString());
  }
}
