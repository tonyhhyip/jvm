package me.tonyhhyip.bibliography.marc.core;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

class SubFieldTest {
  @Test
  void testAll() {
    SubField field = new SubField('a', "Testing");
    assertEquals("$aTesting", field.toString());
    assertEquals('a', field.getCode());
    assertEquals("Testing", field.getData());
  }
}
