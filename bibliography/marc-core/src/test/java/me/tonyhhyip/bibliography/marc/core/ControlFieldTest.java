package me.tonyhhyip.bibliography.marc.core;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

class ControlFieldTest {
  @Test
  void testData() {
    ControlField field = new ControlField("001", "Testing");
    assertEquals("001 Testing", field.toString());
    assertEquals("001", field.getTag());
    assertEquals("Testing", field.getData());
  }

  @Test
  void testCompareTo() {
    ControlField field1 = new ControlField("001", "Testing");
    ControlField field2 = new ControlField("002", "Testing");
    assertEquals(-1, field1.compareTo(field2));
  }
}
