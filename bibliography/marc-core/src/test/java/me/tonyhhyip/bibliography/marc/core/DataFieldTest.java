package me.tonyhhyip.bibliography.marc.core;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

import org.junit.jupiter.api.Test;

class DataFieldTest {
  @Test
  void testData() {
    DataField field = new DataField("245", '0', '1');
    assertEquals("245 01 ", field.toString());
    assertEquals("245", field.getTag());
    assertEquals('0', field.getIndicator1());
    assertEquals('1', field.getIndicator2());
  }

  @Test
  void testSubfield() {
    DataField field = new DataField("245", '0', '1');
    SubField subField1 = new SubField('a', "Testing");
    SubField subField2 = new SubField('b', "Testing For Full");
    field.addSubfield(subField1);
    assertEquals(1, field.getSubfields().size());
    field.removeSubfield(subField1);
    assertEquals(0, field.getSubfields().size());
    field.addSubfield(subField2);
    field.addSubfield(0, subField1);
    assertEquals(subField1, field.getSubfields().get(0));
    assertEquals(1, field.getSubfields('a').size());
    assertNull(field.getSubfield('c'));
    assertNotNull(field.getSubfield('a'));
    assertEquals("245 01 $aTesting$bTesting For Full", field.toString());
  }
}
