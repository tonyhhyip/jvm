package me.tonyhhyip.bibliography.marc.core

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class RecordBuilderKtTest {
    @Test
    fun testRecordBuilder() {
        val record = Record.Builder.create {
            leader("00714cam a2100205 a 4500")
            controlField {
                tag = "001"
                data = "Testing"
            }
            dataField {
                tag = "245"
                indicator1 = '1'
                indicator2 = '0'
                subfield {
                    code = 'a'
                    data = "Testing Book Name"
                }
            }
        }
        assertEquals("00714cam a2100205 a 4500", record.leader.toString())
    }
}
