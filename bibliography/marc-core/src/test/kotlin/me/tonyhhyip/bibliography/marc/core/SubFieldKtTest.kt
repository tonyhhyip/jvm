package me.tonyhhyip.bibliography.marc.core

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class SubFieldKtTest {
    @Test
    fun testAll() {
        val field = SubField('a', "Testing")
        assertEquals("\$aTesting", field.toString())
        assertEquals('a', field.code)
        assertEquals("Testing", field.data)
    }
}
