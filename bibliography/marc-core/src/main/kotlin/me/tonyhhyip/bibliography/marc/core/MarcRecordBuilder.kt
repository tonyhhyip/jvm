package me.tonyhhyip.bibliography.marc.core

@MarcRecordDslMarker
interface MarcRecordBuilder<T> {
    fun build(): T
}
