package me.tonyhhyip.bibliography.marc.core

import java.io.Serializable

open class Record(
    var leader: Leader
) : Serializable {
    companion object {
        private const val LEADER_AS_FIELD = "000"
        private const val CONTROL_NUMBER_FIELD = "001"
        @JvmStatic fun builder() = Builder()
    }

    class Builder : MarcRecordBuilder<Record> {
        companion object {
            @JvmStatic fun create(block: Builder.() -> Unit): Record = Builder().apply(block).build()
        }

        private val fields = mutableListOf<Field>()
        private lateinit var leader: Leader

        fun field(field: Field) {
            fields += field
        }

        fun addField(field: Field) = this.also {
            fields += field
        }

        fun controlField(block: ControlField.Builder.() -> Unit) {
            fields += ControlField.Builder().apply(block).build()
        }

        fun dataField(block: DataField.Builder.() -> Unit) {
            fields += DataField.Builder().apply(block).build()
        }

        fun withLeader(leader: Leader) = this.also {
            this.leader = leader
        }

        fun leader(leader: Leader) {
            this.leader = leader
        }

        fun leader(leader: String) {
            this.leader = Leader(leader)
        }

        fun leader(block: Leader.Builder.() -> Unit) {
            this.leader = Leader.Builder().apply(block).build()
        }

        override fun build(): Record {
            val record = Record(leader)
            fields.forEach(record::addField)
            return record
        }
    }

    val controlFields = mutableListOf<ControlField>()
    val dataFields = mutableListOf<DataField>()

    fun addField(field: Field) {
        when (field) {
            is DataField -> dataFields.add(field)
            is ControlField -> {
                when (field.tag) {
                    LEADER_AS_FIELD -> return
                    CONTROL_NUMBER_FIELD -> {
                        if (controlFields.isNotEmpty() && controlFields[0].tag == CONTROL_NUMBER_FIELD) {
                            controlFields[0] = field
                        } else {
                            controlFields.add(0, field)
                        }
                    }
                    else -> controlFields.add(field)
                }
                if (field.tag == LEADER_AS_FIELD) {
                    // invalid operation, do nothing
                    return
                }
            }
        }
    }

    fun removeField(field: Field) {
        when (field) {
            is DataField -> dataFields.remove(field)
            is ControlField -> controlFields.remove(field)
        }
    }
}
