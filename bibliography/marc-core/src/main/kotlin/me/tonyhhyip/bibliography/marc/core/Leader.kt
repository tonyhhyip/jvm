package me.tonyhhyip.bibliography.marc.core

import java.io.Serializable
import java.util.regex.Pattern

data class Leader(
    val recordLength: Int, // 00 - 04
    val recordStatus: Char, // 05
    val typeOfRecord: Char, // 06
    val bibliographyLevel: Char, // 07
    val typeOfControl: Char, // 08
    val charCodingScheme: Char, // 09
    val indicatorCount: Int, // 10
    val subfieldCodeCount: Int, // 11
    val baseAddressOfData: Int, // 12-16
    val encodingLevel: Char, // 17
    val descriptiveCatalogingForm: Char, // 18
    val multipartResourceRecordLevel: Char, // 19
    val lengthOfLengthOfFieldPortion: Int, // 20
    val lengthOfStartingCharacterPositionPortion: Int, // 21
    val lengthOfImplementationDefinedPortion: Int, // 22
    val placeholder: Char, // 23
) : Serializable {
    companion object {
        @JvmStatic fun build(leader: String) = Leader(leader)
    }

    constructor(leader: String) : this(
        recordLength = if (Pattern.matches("\\d+", leader.substring(0, 5))) {
            Integer.parseInt(leader.substring(0, 5))
        } else {
            0
        },
        recordStatus = leader[5],
        typeOfRecord = leader[6],
        bibliographyLevel = leader[7],
        typeOfControl = leader[8],
        charCodingScheme = leader[9],
        indicatorCount = leader[10] - '0',
        subfieldCodeCount = (leader[11] - '0'),
        baseAddressOfData = Integer.parseInt(leader.substring(12, 17)),
        encodingLevel = leader[17],
        descriptiveCatalogingForm = leader[18],
        multipartResourceRecordLevel = leader[19],
        lengthOfLengthOfFieldPortion = leader[20] - '0',
        lengthOfStartingCharacterPositionPortion = leader[21] - '0',
        lengthOfImplementationDefinedPortion = leader[22] - '0',
        placeholder = leader[23]
    )

    override fun toString(): String {
        return "${String.format("%05d", recordLength)}$recordStatus" +
            "$typeOfRecord$bibliographyLevel$typeOfControl$charCodingScheme" +
            "$indicatorCount$subfieldCodeCount${String.format("%05d",baseAddressOfData)}" +
            "$encodingLevel$descriptiveCatalogingForm$multipartResourceRecordLevel" +
            "$lengthOfLengthOfFieldPortion$lengthOfStartingCharacterPositionPortion" +
            "$lengthOfImplementationDefinedPortion$placeholder"
    }

    class Builder : MarcRecordBuilder<Leader> {
        var recordLength: Int? = null // 00 - 04
        var recordStatus: Char? = null // 05
        var typeOfRecord: Char? = null // 06
        var bibliographyLevel: Char? = null // 07
        var typeOfControl: Char? = null // 08
        var charCodingScheme: Char? = null // 09
        var indicatorCount: Int? = null // 10
        var subfieldCodeCount: Int? = null // 11
        var baseAddressOfData: Int? = null // 12-16
        var encodingLevel: Char? = null // 17
        var descriptiveCatalogingForm: Char? = null // 18
        var multipartResourceRecordLevel: Char? = null // 19
        var lengthOfLengthOfFieldPortion: Int? = null // 20
        var lengthOfStartingCharacterPositionPortion: Int? = null // 21
        var lengthOfImplementationDefinedPortion: Int? = null // 22
        var placeholder: Char? = null // 23

        override fun build() = Leader(
            recordLength!!, // 00 - 04
            recordStatus!!, // 05
            typeOfRecord!!, // 06
            bibliographyLevel!!, // 07
            typeOfControl!!, // 08
            charCodingScheme!!, // 09
            indicatorCount!!, // 10
            subfieldCodeCount!!, // 11
            baseAddressOfData!!, // 12-16
            encodingLevel!!, // 17
            descriptiveCatalogingForm!!, // 18
            multipartResourceRecordLevel!!, // 19
            lengthOfLengthOfFieldPortion!!, // 20
            lengthOfStartingCharacterPositionPortion!!, // 21
            lengthOfImplementationDefinedPortion!!, // 22
            placeholder!!, // 23
        )
    }
}
