package me.tonyhhyip.bibliography.marc.core

import java.io.Serializable

data class SubField(
    val code: Char,
    val data: String,
) : Serializable {
    companion object {
        @JvmStatic fun builder() = Builder()
    }

    override fun toString(): String {
        return "\$$code$data"
    }

    class Builder : MarcRecordBuilder<SubField> {
        var code: Char? = null
        lateinit var data: String

        fun withCode(code: Char) = this.also { it.code = code }
        fun withData(data: String) = this.also { it.data = data }

        override fun build(): SubField {
            requireNotNull(code) { "code cannot be null" }
            return SubField(code!!, data)
        }
    }
}
