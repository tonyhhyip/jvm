package me.tonyhhyip.bibliography.marc.core

import java.io.Serializable

sealed class Field(
    private val tag: String,
) : Serializable, Comparable<Field> {
    override fun toString(): String = tag

    override fun compareTo(other: Field): Int {
        return toString().compareTo(other.toString())
    }
}

data class ControlField(
    val tag: String,
    val data: String,
) : Field(tag) {
    companion object {
        @JvmStatic fun builder() = Builder()
    }

    override fun toString(): String {
        return "${super.toString()} $data"
    }

    class Builder : MarcRecordBuilder<ControlField> {
        lateinit var tag: String
        lateinit var data: String

        fun withTag(tag: String) = this.also { it.tag = tag }

        fun withData(data: String) = this.also { it.data = data }

        override fun build() = ControlField(tag, data)
    }
}

data class DataField(
    val tag: String,
    val indicator1: Char,
    val indicator2: Char,
) : Field(tag) {
    private val subfields = mutableListOf<SubField>()

    fun addSubfield(subfield: SubField) {
        subfields.add(subfield)
    }

    fun addSubfield(index: Int, subfield: SubField) {
        subfields.add(index, subfield)
    }

    fun removeSubfield(subfield: SubField) {
        subfields.remove(subfield)
    }

    fun getSubfields(): List<SubField> {
        return subfields
    }

    /**
     * Returns the {@link List} of {@link Subfield}s for the given subfield code.
     *
     * @param code The code of the subfields to return
     * @return The {@link List} of {@link Subfield}s in the <code>DataField</code>
     */
    fun getSubfields(code: Char): List<SubField> {
        return subfields.filter { it.code == code }
    }

    /**
     * Returns the first <code>Subfield</code> with the given code.
     *
     * @param code The subfield code of the <code>Subfield</code> to return
     * @return The <code>Subfield</code> or null if no subfield is found
     */
    fun getSubfield(code: Char): SubField? {
        return subfields.firstOrNull { it.code == code }
    }

    override fun toString(): String {
        return "${super.toString()} $indicator1$indicator2 ${subfields.joinToString("") { it.toString() }}"
    }

    companion object {
        @JvmStatic fun builder() = Builder()
    }

    class Builder : MarcRecordBuilder<DataField> {
        lateinit var tag: String
        var indicator1: Char = ' '
        var indicator2: Char = ' '
        private val fields = mutableListOf<SubField>()

        fun withTag(tag: String) = this.also { it.tag = tag }
        fun withIndicator1(indicator: Char) = this.also { it.indicator1 = indicator }
        fun withIndicator2(indicator: Char) = this.also { it.indicator2 = indicator }

        fun addSubfield(subfield: SubField) = this.apply {
            fields += subfield
        }
        fun subfield(block: SubField.Builder.() -> Unit) = SubField.Builder().apply(block).build()

        override fun build(): DataField {
            val dataField = DataField(
                tag,
                indicator1,
                indicator2,
            )
            fields.forEach(dataField::addSubfield)
            return dataField
        }
    }
}
