plugins {
    jacoco
    `maven-publish`
    kotlin("jvm")
}

val ktlintVersion: String by rootProject.extra
val jacocoVersion: String by rootProject.extra

spotless {
    kotlin {
        ktlint(ktlintVersion)
    }
}

jacoco {
    toolVersion = jacocoVersion
}

dependencies {
    implementation(kotlin("stdlib-jdk8"))
    api(project(":bibliography:marc-serialize-api"))
    implementation("javax.xml.bind:jaxb-api:2.3.1")
    testImplementation("com.sun.xml.bind:jaxb-impl:2.3.3")
    testImplementation("org.junit.jupiter:junit-jupiter-api:5.7.0")
    testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine:5.7.0")
}

java {
    withSourcesJar()
}

tasks.withType<Test> {
    finalizedBy(tasks.jacocoTestReport)
}

tasks.jacocoTestReport {
    dependsOn(tasks.test)
    reports {
        xml.required.set(true)
        xml.outputLocation.set(file("$buildDir/jacoco/jacoco.xml"))
        html.outputLocation.set(file("$buildDir/jacoco/html"))
    }
}

publishing {
    repositories {
        maven {
            val gitlabToken: String? by project
            name = "gitlab"
            url = uri("https://gitlab.com/api/v4/projects/22919357/packages/maven")
            credentials(HttpHeaderCredentials::class) {
                name = "Private-Token"
                value = gitlabToken
            }
            authentication {
                create<HttpHeaderAuthentication>("header")
            }
        }
    }
    publications {
        create<MavenPublication>("maven") {
            groupId = "me.tonyhhyip.jvm.bibliography.marc"
            artifactId = "serialize-xml-jaxb"
            version = "1.0.0-SANPSHOT"

            from(components["java"])
            pom {
                name.set("marc serialize api")
                licenses {
                    license {
                        name.set("GNU Lesser General Public License version 3")
                        url.set("https://opensource.org/licenses/LGPL-3.0")
                    }
                }
                developers {
                    developer {
                        id.set("tonyhhyip")
                        name.set("Tony Yip")
                        email.set("tony@tonyhhyip.me")
                    }
                }
            }
        }
    }
}
