//
// This file was generated by the Eclipse Implementation of JAXB, v2.3.3
// See https://eclipse-ee4j.github.io/jaxb-ri
// Any modifications to this file will be lost upon recompilation of the source schema.
// Generated on: 2020.12.28 at 06:26:08 PM HKT
//

@javax.xml.bind.annotation.XmlSchema(
    namespace = "http://www.loc.gov/MARC21/slim",
    elementFormDefault = javax.xml.bind.annotation.XmlNsForm.QUALIFIED)
package me.tonyhhyip.bibliography.marc.serialize.xml.jaxb.binding;
