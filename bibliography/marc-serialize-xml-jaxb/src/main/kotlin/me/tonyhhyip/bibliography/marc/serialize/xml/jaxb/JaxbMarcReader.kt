package me.tonyhhyip.bibliography.marc.serialize.xml.jaxb

import me.tonyhhyip.bibliography.marc.core.ControlField
import me.tonyhhyip.bibliography.marc.core.DataField
import me.tonyhhyip.bibliography.marc.core.Leader
import me.tonyhhyip.bibliography.marc.core.Record
import me.tonyhhyip.bibliography.marc.core.SubField
import me.tonyhhyip.bibliography.marc.serialize.MarcReader
import me.tonyhhyip.bibliography.marc.serialize.xml.jaxb.binding.Collection
import me.tonyhhyip.bibliography.marc.serialize.xml.jaxb.binding.MarcRecord
import org.xml.sax.InputSource
import java.io.InputStream

object JaxbMarcReader : MarcReader {
    override val fileExtension = "xml"
    override val format = "marcxml"

    override fun parse(inputStream: InputStream): Iterable<Record> {
        val inputSource = InputSource(inputStream)
        return when (val record = unmarshaller.unmarshal(inputSource)) {
            is Collection -> record.record
            is MarcRecord -> listOf(record)
            else -> throw RuntimeException("unable to unmarshal input")
        }.map {
            Record(
                Leader(it.leader.value)
            )
                .apply {
                    it.controlfield
                        .map { field ->
                            ControlField(field.tag, field.value)
                        }
                        .forEach(::addField)
                    it.datafield.map { field ->
                        DataField(field.tag, field.ind1[0], field.ind2[0]).apply {
                            field.subfield.map { subField ->
                                SubField(
                                    subField.code[0],
                                    subField.value
                                )
                            }
                                .forEach(::addSubfield)
                        }
                    }.forEach(::addField)
                }
        }
    }
}
