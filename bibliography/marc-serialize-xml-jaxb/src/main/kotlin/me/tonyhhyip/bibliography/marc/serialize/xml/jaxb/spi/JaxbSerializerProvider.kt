package me.tonyhhyip.bibliography.marc.serialize.xml.jaxb.spi

import me.tonyhhyip.bibliography.marc.serialize.MarcReader
import me.tonyhhyip.bibliography.marc.serialize.MarcWriter
import me.tonyhhyip.bibliography.marc.serialize.spi.SerializerProvider
import me.tonyhhyip.bibliography.marc.serialize.xml.jaxb.JaxbMarcReader
import me.tonyhhyip.bibliography.marc.serialize.xml.jaxb.JaxbMarcWriter

class JaxbSerializerProvider : SerializerProvider {
    override fun reader(): MarcReader = JaxbMarcReader
    override fun writer(): MarcWriter = JaxbMarcWriter
}
