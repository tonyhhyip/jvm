package me.tonyhhyip.bibliography.marc.serialize.xml.jaxb

import me.tonyhhyip.bibliography.marc.serialize.xml.jaxb.binding.Collection
import me.tonyhhyip.bibliography.marc.serialize.xml.jaxb.binding.MarcRecord
import javax.xml.bind.JAXBContext

internal val context by lazy {
    JAXBContext.newInstance(
        MarcRecord::class.java,
        Collection::class.java
    )
}
internal val unmarshaller by lazy { context.createUnmarshaller() }
internal val marshaller by lazy { context.createMarshaller() }
