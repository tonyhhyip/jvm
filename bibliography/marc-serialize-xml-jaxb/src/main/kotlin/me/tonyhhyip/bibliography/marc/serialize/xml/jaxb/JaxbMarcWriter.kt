package me.tonyhhyip.bibliography.marc.serialize.xml.jaxb

import me.tonyhhyip.bibliography.marc.core.Record
import me.tonyhhyip.bibliography.marc.serialize.MarcWriter
import me.tonyhhyip.bibliography.marc.serialize.xml.jaxb.binding.Collection
import me.tonyhhyip.bibliography.marc.serialize.xml.jaxb.binding.ControlField
import me.tonyhhyip.bibliography.marc.serialize.xml.jaxb.binding.DataField
import me.tonyhhyip.bibliography.marc.serialize.xml.jaxb.binding.LeaderField
import me.tonyhhyip.bibliography.marc.serialize.xml.jaxb.binding.MarcRecord
import me.tonyhhyip.bibliography.marc.serialize.xml.jaxb.binding.SubField
import java.io.OutputStream

object JaxbMarcWriter : MarcWriter {
    override val fileExtension = "xml"
    override val format = "marcxml"

    override fun write(output: OutputStream, records: kotlin.collections.Collection<Record>) {
        val collection = Collection().apply {
            record = records.map { record ->
                MarcRecord().apply {
                    leader = LeaderField().apply {
                        value = record.leader.toString()
                    }
                    controlfield = record.controlFields.map { controlField ->
                        ControlField().apply {
                            tag = controlField.tag
                            value = controlField.data
                        }
                    }
                    datafield = record.dataFields.map { dataField ->
                        DataField().apply {
                            tag = dataField.tag
                            ind1 = dataField.indicator1.toString()
                            ind2 = dataField.indicator2.toString()
                            subfield = dataField.getSubfields().map { subfield ->
                                SubField().apply {
                                    tag = subfield.code.toString()
                                    value = subfield.data
                                }
                            }
                        }
                    }
                }
            }
        }
        marshaller.marshal(collection, output)
    }
}
