package me.tonyhhyip.bibliography.marc.serialize.xml.jaxb;

import java.util.Collections;
import me.tonyhhyip.bibliography.marc.core.ControlField;
import me.tonyhhyip.bibliography.marc.core.DataField;
import me.tonyhhyip.bibliography.marc.core.Leader;
import me.tonyhhyip.bibliography.marc.core.Record;
import me.tonyhhyip.bibliography.marc.core.SubField;
import me.tonyhhyip.bibliography.marc.serialize.MarcWriter;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class JaxbMarcWriterTest {
  @Test
  void testWrite() {
    MarcWriter writer = JaxbMarcWriter.INSTANCE;
    Record record =
        Record.builder()
            .withLeader(Leader.build("00714cam a2100205 a 4500"))
            .addField(ControlField.builder().withTag("001").withData("Testing").build())
            .addField(
                DataField.builder()
                    .withTag("245")
                    .withIndicator1('1')
                    .withIndicator2('0')
                    .addSubfield(
                        SubField.builder().withCode('a').withData("Testing Book Name").build())
                    .build())
            .build();
    String output = writer.writeAsString(Collections.singletonList(record));
    Assertions.assertTrue(output.startsWith("<?xml"));
  }
}
