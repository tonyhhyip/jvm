package me.tonyhhyip.bibliography.marc.serialize.xml.jaxb.spi;

import me.tonyhhyip.bibliography.marc.serialize.spi.SerializerProvider;
import me.tonyhhyip.bibliography.marc.serialize.xml.jaxb.JaxbMarcReader;
import me.tonyhhyip.bibliography.marc.serialize.xml.jaxb.JaxbMarcWriter;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class JaxbSerializerProviderTest {
  @Test
  void testProvide() {
    SerializerProvider provider = new JaxbSerializerProvider();
    Assertions.assertEquals(JaxbMarcReader.INSTANCE, provider.reader());
    Assertions.assertEquals(JaxbMarcWriter.INSTANCE, provider.writer());
  }
}
