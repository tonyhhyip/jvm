package me.tonyhhyip.bibliography.marc.serialize.xml.jaxb;

import java.io.InputStream;
import me.tonyhhyip.bibliography.marc.core.Record;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class JaxbMarcReaderTest {
  @Test
  void testConstant() {
    Assertions.assertEquals("marcxml", JaxbMarcReader.INSTANCE.getFormat());
    Assertions.assertEquals("xml", JaxbMarcReader.INSTANCE.getFileExtension());
  }

  @Test
  void testReadCollection() {
    InputStream input = this.getClass().getResourceAsStream("/chabon.xml");
    int count = 0;
    for (Record record : JaxbMarcReader.INSTANCE.parse(input)) {
      count += 1;
      if (count == 1) {
        Assertions.assertEquals("00759cam a2200229 a 4500", record.getLeader().toString());
      } else {
        Assertions.assertEquals("00714cam a2200205 a 4500", record.getLeader().toString());
      }
    }
    Assertions.assertEquals(2, count);
  }

  @Test
  void testReadRecord() {
    InputStream input = this.getClass().getResourceAsStream("/summerland-record-with-type.xml");
    int count = 0;
    for (Record record : JaxbMarcReader.INSTANCE.parse(input)) {
      count += 1;
      Assertions.assertEquals("00714cam a2200205 a 4500", record.getLeader().toString());
    }
    Assertions.assertEquals(1, count);
  }
}
