plugins {
    jacoco
    `maven-publish`
    kotlin("jvm")
}

dependencies {
    implementation(kotlin("stdlib-jdk8"))
    api(project(":bibliography:marc-core"))
    testImplementation(platform("org.junit:junit-bom:5.7.2"))
    testImplementation("org.junit.jupiter:junit-jupiter-api")
    testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine")
}

val ktlintVersion: String by rootProject.extra
val jacocoVersion: String by rootProject.extra

spotless {
    kotlin {
        ktlint(ktlintVersion)
    }
}

jacoco {
    toolVersion = jacocoVersion
}

java {
    withSourcesJar()
}

tasks.jacocoTestReport {
    dependsOn(tasks.test)
    reports {
        xml.required.set(true)
        xml.outputLocation.set(file("$buildDir/jacoco/jacoco.xml"))
        html.outputLocation.set(file("$buildDir/jacoco/html"))
    }
}

tasks.withType<Test> {
    useJUnitPlatform()
    finalizedBy(tasks.jacocoTestReport)
}

publishing {
    repositories {
        maven {
            val gitlabToken: String? by project
            name = "gitlab"
            url = uri("https://gitlab.com/api/v4/projects/22919357/packages/maven")
            credentials(HttpHeaderCredentials::class) {
                name = "Private-Token"
                value = gitlabToken
            }
            authentication {
                create<HttpHeaderAuthentication>("header")
            }
        }
    }
    publications {
        create<MavenPublication>("maven") {
            groupId = "me.tonyhhyip.jvm.bibliography.marc"
            artifactId = "serialize-api"
            version = "1.0.0-SANPSHOT"

            from(components["java"])
            pom {
                name.set("marc serialize api")
                licenses {
                    license {
                        name.set("GNU Lesser General Public License version 3")
                        url.set("https://opensource.org/licenses/LGPL-3.0")
                    }
                }
                developers {
                    developer {
                        id.set("tonyhhyip")
                        name.set("Tony Yip")
                        email.set("tony@tonyhhyip.me")
                    }
                }
            }
        }
    }
}
