package me.tonyhhyip.bibliography.marc.serialize.spi

import me.tonyhhyip.bibliography.marc.serialize.MarcReader
import me.tonyhhyip.bibliography.marc.serialize.MarcWriter

interface SerializerProvider {
    fun reader(): MarcReader
    fun writer(): MarcWriter
}
