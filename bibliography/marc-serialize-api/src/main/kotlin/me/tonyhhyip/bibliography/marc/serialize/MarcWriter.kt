package me.tonyhhyip.bibliography.marc.serialize

import me.tonyhhyip.bibliography.marc.core.Record
import java.io.ByteArrayOutputStream
import java.io.OutputStream

interface MarcWriter {
    val fileExtension: String
    val format: String
    fun write(output: OutputStream, records: Collection<Record>)
    fun writeAsBytes(records: Collection<Record>): ByteArray {
        val output = ByteArrayOutputStream()
        write(output, records)
        return output.toByteArray()
    }
    fun writeAsString(records: Collection<Record>) = String(writeAsBytes(records))
}
