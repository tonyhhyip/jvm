package me.tonyhhyip.bibliography.marc.serialize

import me.tonyhhyip.bibliography.marc.core.Record
import java.io.InputStream

interface MarcReader {
    val fileExtension: String
    val format: String
    fun parse(inputStream: InputStream): Iterable<Record>
}
