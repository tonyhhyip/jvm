package me.tonyhhyip.bibliography.marc.serialize.mrk8

import me.tonyhhyip.bibliography.marc.core.ControlField
import me.tonyhhyip.bibliography.marc.core.DataField
import me.tonyhhyip.bibliography.marc.core.Field
import me.tonyhhyip.bibliography.marc.core.Leader
import me.tonyhhyip.bibliography.marc.core.Record
import me.tonyhhyip.bibliography.marc.core.SubField
import me.tonyhhyip.bibliography.marc.serialize.MarcReader
import java.io.BufferedInputStream
import java.io.InputStream
import java.nio.charset.StandardCharsets
import java.util.Scanner

object Mrk8MarcReader : MarcReader {
    override val fileExtension = "mrk8"
    override val format = "mrk8"

    override fun parse(inputStream: InputStream): Iterable<Record> = Iterable {
        MarcIterator(
            Scanner(
                if (inputStream is BufferedInputStream) {
                    inputStream
                } else {
                    BufferedInputStream(inputStream)
                },
                StandardCharsets.UTF_8
            )
        )
    }

    private class MarcIterator(private val scanner: Scanner) : Iterator<Record> {
        companion object {
            private fun isControlField(tag: String): Boolean {
                return ((tag.length == 3) && tag.startsWith("00") && (tag[2] >= '0') && (tag[2] <= '9'))
            }

            private fun unescapeFieldValue(value: String): String {
                return value.replace('\\', ' ')
            }

            private fun isValidIndicator(indicator: Char) = (indicator == ' ' || (indicator in '0'..'9'))
        }

        private var lastLineRead: String? = null

        override fun hasNext() = scanner.hasNextLine()

        override fun next(): Record {
            val lines = mutableListOf<String>()
            if (this.lastLineRead != null && this.lastLineRead!!.substring(1, 4).equals("LDR", ignoreCase = true)) {
                lines += this.lastLineRead!!
                this.lastLineRead = null
            }
            while (scanner.hasNextLine()) {
                val line = scanner.nextLine()
                if (line.trim().isEmpty()) {
                    continue
                }
                if (line.substring(1, 4).equals("LDR", ignoreCase = true) && lines.size > 0) {
                    this.lastLineRead = line
                    break
                }
                lines += line
            }
            return this.parse(lines)
        }

        fun parse(lines: List<String>): Record {
            require(lines.isNotEmpty())

            val record = Record(Leader(lines[0].substring(6)))

            lines.drop(0).forEach { line ->
                val tag = line.substring(1, 4)
                val field: Field = if (isControlField(tag)) {
                    ControlField(tag, unescapeFieldValue(line.substring(6)))
                } else {
                    val data = line.substring(6)
                    val ind1 = if (data[0] == '\\') ' ' else data[0]
                    val ind2 = if (data[1] == '\\') ' ' else data[1]
                    if (!isValidIndicator(ind1) || !isValidIndicator(ind2)) {
                        throw InvalidMrk8FormatException("Wrong indicator format. It has to be a number or a space")
                    }
                    DataField(tag, ind1, ind2).apply {
                        data.substring(3).split("\\$")
                            .forEach { this.addSubfield(SubField(it[0], Mrk8TranslationTable.fromMrk8(it.substring(1)))) }
                    }
                }
                record.addField(field)
            }
            return record
        }
    }
}
