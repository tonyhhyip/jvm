package me.tonyhhyip.bibliography.marc.serialize.mrk8

import java.lang.RuntimeException

class InvalidMrk8FormatException(message: String) : RuntimeException(message)
