/*******************************************************************
*  TEXT-TO-MARC 21                      Date: October 7, 2003      *
*  CHARACTER CONVERSION TABLE FOR MARCMaker Version 2.5            *
*  Includes mnemonics for all Latin-1 (Windows 1252) and Latin-2   *
*  (Windows 1250 with letter-with-diacritic characters used in     *
*  Eastern and Western European languages. Also includes mnemonics *
*  used for the ALA-LC romanization tables for Russian and         *
*  Ukrainian                                                       *
*                                                                  *
*  Source: Randall K. BARRY                                        *
*          U.S. Library of Congress                                *
*          Network Development and MARC Standards Office           *
*          101 Independence Ave., S.E.                             *
*          Washington, D.C. 20540-4402  U.S.A.                     *
*          TEL: +1-202-707-5118                                    *
*          FAX: +1-202-707-0115                                    *
*          NET: rbar@loc.gov                                       *
*                                                                  *
* Converts any listed mnemonic to its hexadecimal equivalent.      *
*                                                                  *
* The curly braces "{" (7Bx/123d) and "}" (7Dx/125d) are used      *
* in MARCMaker to delimit mnemonics converted according to this    *
* table.  Any characters not delimited by the curly braces are     *
* passed unchanged into the MARC output record.                    *
*                                                                  *
* Mnemonics encountered that are not listed in this table are      *
* passed to the output record preceded by an ampersand (&) and     *
* followed by a semicolon (;).  Thus "{zilch}" would be come       *
* "&zilch;" in the output record.                                  *
*                                                                  *
* Mnemonics in this table are enclosed in curly braces "{...}".    *
* When a mnemonic is mapped to more than one character, the        *
* character codes are separated by a space in this table.          *
*                                                                  *
* Columns in this table are delimited by a comma ","               *
*                                                                  *
********************************************************************
* Mnemonic, hex value // name/comment                              *
* {**},     ??                // [character name (FORMAT)]         *
*__________________________________________________________________*/

package me.tonyhhyip.bibliography.marc.serialize.mrk8

internal object Mrk8TranslationTable {
    private val mrk8Table: Map<String, String>

    init {
        val tables = mapOf(
            "{0}" to "30x", // zero
            "{00}" to "00x", // hex value 00
            "{01}" to "01x", // hex value 01
            "{02}" to "02x", // hex value 02
            "{03}" to "03x", // hex value 03
            "{04}" to "04x", // hex value 04
            "{05}" to "05x", // hex value 05
            "{06}" to "06x", // hex value 06
            "{07}" to "07x", // hex value 07
            "{08}" to "08x", // hex value 08
            "{09}" to "09x", // hex value 09
            "{0A}" to "0Ax", // hex value 0A
            "{0B}" to "0Bx", // hex value 0B
            "{0C}" to "0Cx", // hex value 0C
            "{0D}" to "0Dx", // hex value 0D
            "{0E}" to "0Ex", // hex value 0E
            "{0F}" to "0Fx", // hex value 0F
            "{1}" to "31x", // digit one
            "{10}" to "10x", // hex value 10
            "{11}" to "11x", // hex value 11
            "{12}" to "12x", // hex value 12
            "{13}" to "13x", // hex value 13
            "{14}" to "14x", // hex value 14
            "{15}" to "15x", // hex value 15
            "{16}" to "16x", // hex value 16
            "{17}" to "17x", // hex value 17
            "{18}" to "18x", // hex value 18
            "{19}" to "19x", // hex value 19
            "{1A}" to "1Ax", // hex value 1A
            "{1B}" to "1Bx", // escape
            "{1C}" to "1Cx", // hex value 1C
            "{1D}" to "1Dx", // end of record
            "{1E}" to "1Ex", // end of field
            "{1F}" to "1Fx", // subfield delimiter
            "{2}" to "32x", // digit two
            "{20}" to "20x", // space (blank)
            "{21}" to "21x", // exclamation point
            "{22}" to "22x", // quotation mark
            "{23}" to "23x", // number sign
            "{24}" to "24x", // dollar sign
            "{25}" to "25x", // percent sign
            "{26}" to "26x", // ampersand
            "{27}" to "27x", // apostrophe
            "{28}" to "28x", // left parenthesis
            "{29}" to "29x", // right parenthesis
            "{2A}" to "2Ax", // asterisk
            "{2B}" to "2Bx", // plus
            "{2C}" to "2Cx", // comma
            "{2D}" to "2Dx", // hyphen-minus
            "{2E}" to "2Ex", // period/decimal point
            "{2F}" to "2Fx", // solidus (slash)
            "{3}" to "33x", // digit three
            "{30}" to "30x", // digit zero
            "{31}" to "31x", // digit one
            "{32}" to "32x", // digit two
            "{33}" to "33x", // digit three
            "{34}" to "34x", // digit four
            "{35}" to "35x", // digit five
            "{36}" to "36x", // digit six
            "{37}" to "37x", // digit seven
            "{38}" to "38x", // digit eight
            "{39}" to "39x", // digit nine
            "{3A}" to "3Ax", // colon
            "{3B}" to "3Bx", // semicolon
            "{3C}" to "3Cx", // less than
            "{3D}" to "3Dx", // equals sign
            "{3E}" to "3Ex", // greater than
            "{3F}" to "3Fx", // question mark
            "{4}" to "34x", // digit four
            "{40}" to "40x", // commercial at sign
            "{41}" to "41x", // latin large letter a
            "{42}" to "42x", // latin large letter b
            "{43}" to "43x", // latin large letter c
            "{44}" to "44x", // latin large letter d
            "{45}" to "45x", // latin large letter e
            "{46}" to "46x", // latin large letter f
            "{47}" to "47x", // latin large letter g
            "{48}" to "48x", // latin large letter h
            "{49}" to "49x", // latin large letter i
            "{4A}" to "4Ax", // latin large letter j
            "{4B}" to "4Bx", // latin large letter k
            "{4C}" to "4Cx", // latin large letter l
            "{4D}" to "4Dx", // latin large letter m
            "{4E}" to "4Ex", // latin large letter n
            "{4F}" to "4Fx", // latin large letter o
            "{5}" to "35x", // digit five
            "{50}" to "50x", // latin large letter p
            "{51}" to "51x", // latin large letter q
            "{52}" to "52x", // latin large letter r
            "{53}" to "53x", // latin large letter s
            "{54}" to "54x", // latin large letter t
            "{55}" to "55x", // latin large letter u
            "{56}" to "56x", // latin large letter v
            "{57}" to "57x", // latin large letter w
            "{58}" to "58x", // latin large letter x
            "{59}" to "59x", // latin large letter y
            "{5A}" to "5Ax", // latin large letter z
            "{5B}" to "5Bx", // left bracket
            "{5C}" to "5Cx", // back slash (reverse solidus)
            "{5D}" to "5Dx", // right bracket
            "{5E}" to "5Ex", // spacing circumflex
            "{5F}" to "5Fx", // spacing underscore
            "{6}" to "36x", // digit six
            "{60}" to "60x", // spacing grave
            "{61}" to "61x", // latin small letter a
            "{62}" to "62x", // latin small letter b
            "{63}" to "63x", // latin small letter c
            "{64}" to "64x", // latin small letter d
            "{65}" to "65x", // latin small letter e
            "{66}" to "66x", // latin small letter f
            "{67}" to "67x", // latin small letter g
            "{68}" to "68x", // latin small letter h
            "{69}" to "69x", // latin small letter i
            "{6A}" to "6Ax", // latin small letter j
            "{6B}" to "6Bx", // latin small letter k
            "{6C}" to "6Cx", // latin small letter l
            "{6D}" to "6Dx", // latin small letter m
            "{6E}" to "6Ex", // latin small letter n
            "{6F}" to "6Fx", // latin small letter o
            "{7}" to "37x", // digit seven
            "{70}" to "70x", // latin small letter p
            "{71}" to "71x", // latin small letter q
            "{72}" to "72x", // latin small letter r
            "{73}" to "73x", // latin small letter s
            "{74}" to "74x", // latin small letter t
            "{75}" to "75x", // latin small letter u
            "{76}" to "76x", // latin small letter v
            "{77}" to "77x", // latin small letter w
            "{78}" to "78x", // latin small letter x
            "{79}" to "79x", // latin small letter y
            "{7A}" to "7Ax", // latin small letter z
            "{7B}" to "7Bx", // opening curly brace
            "{7C}" to "7Cx", // fill/bar over bar/pipe
            "{7D}" to "7Dx", // closing curly brace
            "{7E}" to "7Ex", // spacing tilde
            "{7F}" to "7Fx", // hex value 7F
            "{8}" to "38x", // digit eight
            "{80}" to "80x", // hex value 80
            "{81}" to "81x", // hex value 81
            "{82}" to "82x", // hex value 82
            "{83}" to "83x", // hex value 83
            "{84}" to "84x", // hex value 84
            "{85}" to "85x", // hex value 85
            "{86}" to "86x", // hex value 86
            "{87}" to "87x", // hex value 87
            "{88}" to "88x", // hex value 88
            "{89}" to "89x", // hex value 89
            "{8A}" to "8Ax", // hex value 8A
            "{8B}" to "8Bx", // hex value 8B
            "{8C}" to "8Cx", // hex value 8C
            "{8D}" to "8Dx", // zero width joiner
            "{8E}" to "8Ex", // zero width non-joiner
            "{8F}" to "8Fx", // hex value 8F
            "{9}" to "39x", // digit nine
            "{90}" to "90x", // hex value 90
            "{91}" to "91x", // hex value 91
            "{92}" to "92x", // hex value 92
            "{93}" to "93x", // hex value 93
            "{94}" to "94x", // hex value 94
            "{95}" to "95x", // hex value 95
            "{96}" to "96x", // hex value 96
            "{97}" to "97x", // hex value 97
            "{98}" to "98x", // hex value 98
            "{99}" to "99x", // hex value 99
            "{9A}" to "9Ax", // hex value 9A
            "{9B}" to "9Bx", // hex value 9B
            "{9C}" to "9Cx", // hex value 9C
            "{9D}" to "9Dx", // hex value 9D
            "{9E}" to "9Ex", // hex value 9E
            "{9F}" to "9Fx", // hex value 9F
            "{A}" to "41x", // latin large letter a
            "{a}" to "61x", // latin small letter a
            "{A0}" to "A0x", // no-break space
            "{A1}" to "A1x", // latin large letter l with stroke
            "{A2}" to "A2x", // latin large letter o with stroke
            "{A3}" to "A3x", // latin large letter d with stroke
            "{A4}" to "A4x", // latin large letter thorn
            "{A5}" to "A5x", // latin large letter AE
            "{A6}" to "A6x", // latin large letter OE
            "{A7}" to "A7x", // modifier letter prime/soft sign
            "{A8}" to "A8x", // middle dot
            "{A9}" to "A9x", // musical flat sign
            "{AA}" to "AAx", // registered sign
            "{Aacute}" to "E2x 41x", // latin large letter a with acute
            "{aacute}" to "E2x 61x", // latin small letter a with acute
            "{AB}" to "ABx", // plus-minus sign
            "{Abreve}" to "E6x 41x", // latin large letter a with breve
            "{abreve}" to "E6x 61x", // latin small letter a with breve
            "{AC}" to "ACx", // latin large letter o with horn
            "{Acirc}" to "E3x 41x", // latin large letter a with circumflex
            "{acirc}" to "E3x 61x", // latin small letter a with circumflex
            "{acute}" to "E2x", // combining acute
            "{Acy}" to "41x", // cyrillic large letter a
            "{acy}" to "61x", // cyrillic small letter a
            "{AD}" to "ADx", // latin large letter u with horn
            "{AE}" to "AEx", // modifier letter right half ring/alif
            "{AElig}" to "A5x", // latin large letter AE
            "{aelig}" to "B5x", // latin small letter ae
            "{AF}" to "AFx", // hex value AF
            "{agr}" to "61x", // greek small letter alpha
            "{Agrave}" to "E1x 41x", // latin large letter a with grave
            "{agrave}" to "E1x 61x", // latin small letter a with grave
            "{alif}" to "AEx", // modifier letter right half ring (alif)
            "{amp}" to "26x", // ampersand
            "{Aogon}" to "F1x 41x", // latin large letter a with ogon (hook right)
            "{aogon}" to "F1x 61x", // latin small letter a with ogon (hook right)
            "{apos}" to "27x", // apostrophe
            "{arab}" to "28x 33x", // begin arabic script
            "{Aring}" to "EAx 41x", // latin large letter a with ring
            "{aring}" to "EAx 61x", // latin small letter a with ring
            "{ast}" to "2Ax", // asterisk
            "{asuper}" to "61x", // superscript a
            "{Atilde}" to "E4x 41x", // latin large letter A with tilde
            "{atilde}" to "E4x 61x", // latin small letter a with tilde
            "{Auml}" to "E8x 41x", // latin large letter A with umlaut
            "{auml}" to "E8x 61x", // latin small letter a with umlaut
            "{ayn}" to "B0x", // modifier letter left half ring (ayn)
            "{B}" to "42x", // latin large letter b
            "{b}" to "62x", // latin small letter b
            "{B0}" to "B0x", // modifier letter left half ring/ayn
            "{B1}" to "B1x", // latin small letter l with stroke
            "{B2}" to "B2x", // latin small letter o with stroke
            "{B3}" to "B3x", // latin small letter d with stroke
            "{B4}" to "B4x", // latin small letter thorn
            "{B5}" to "B5x", // latin small letter ae
            "{B6}" to "B6x", // latin small letter oe
            "{B7}" to "B7x", // modifier letter double prime/hard sign
            "{B8}" to "B8x", // latin small letter dotless i
            "{B9}" to "B9x", // pound sign
            "{BA}" to "BAx", // latin small letter eth
            "{BB}" to "BBx", // hex value BB
            "{BC}" to "BCx", // latin small letter o with horn
            "{bcy}" to "62x", // cyrillic small letter be
            "{Bcy}" to "42x", // cyrillic large letter be
            "{BD}" to "BDx", // latin small letter u with horn
            "{BE}" to "BEx", // hex value BE
            "{BF}" to "BFx", // hex value BF
            "{bgr}" to "62x", // greek small letter beta
            "{breve}" to "E6x", // combining breve
            "{breveb}" to "F9x", // combining breve below
            "{brvbar}" to "7Cx", // broken vertical bar
            "{bsol}" to "5Cx", // reverse solidus (back slash)
            "{bull}" to "2Ax", // bullet
            "{C}" to "43x", // latin large letter c
            "{c}" to "63x", // latin small letter c
            "{C0}" to "C0x", // degree sign
            "{C1}" to "C1x", // latin small letter script l
            "{C2}" to "C2x", // sound recording copyright
            "{C3}" to "C3x", // copyright sign
            "{C4}" to "C4x", // sharp
            "{C5}" to "C5x", // inverted question mark
            "{C6}" to "C6x", // inverted exclamation mark
            "{C7}" to "C7x", // hex value C7
            "{C8}" to "C8x", // hex value C8
            "{C9}" to "C9x", // hex value C9
            "{CA}" to "CAx", // hex value CA
            "{Cacute}" to "E2x 43x", // latin large letter c with acute
            "{cacute}" to "E2x 63x", // latin small letter c with acute
            "{candra}" to "EFx", // combining candrabindu
            "{caron}" to "E9x", // combining hacek
            "{CB}" to "CBx", // hex value CB
            "{CC}" to "CCx", // hex value CC
            "{Ccaron}" to "E9x 43x", // latin large letter c with caron
            "{ccaron}" to "E9x 63x", // latin small letter c with caron
            "{Ccedil}" to "F0x 43x", // latin large letter c with cedilla
            "{ccedil}" to "F0x 63x", // latin small letter c with cedilla
            "{CD}" to "CDx", // hex value CD
            "{CE}" to "CEx", // hex value CE
            "{cedil}" to "F0x", // combining cedilla
            "{cent}" to "63x", // cent sign
            "{CF}" to "CFx", // hex value CF
            "{CHcy}" to "43x 68x", // cyrillic large letter cha
            "{chcy}" to "63x 68x", // cyrillic small letter cha
            "{circ}" to "E3x", // combining circumflex
            "{circb}" to "F4x", // combining circumflex below
            "{cjk}" to "24x 31x", // begin chinese japanese korean script
            "{colon}" to "3Ax", // colon
            "{comma}" to "2Cx", // comma
            "{commaa}" to "FEx", // combining comma above
            "{commab}" to "F7x", // combining comma below (hook left)
            "{commat}" to "40x", // commercial at sign
            "{copy}" to "C3x", // copyright sign
            "{curren}" to "2Ax", // currency sign
            "{cyril}" to "28x 4Ex", // begin cyrillic script
            "{D}" to "44x", // latin large letter d
            "{d}" to "64x", // latin small letter d
            "{D0}" to "D0x", // hex value D0
            "{D1}" to "D1x", // hex value D1
            "{D2}" to "D2x", // hex value D2
            "{D3}" to "D3x", // hex value D3
            "{D4}" to "D4x", // hex value D4
            "{D5}" to "D5x", // hex value D5
            "{D6}" to "D6x", // hex value D6
            "{D7}" to "D7x", // hex value D7
            "{D8}" to "D8x", // hex value D8
            "{D9}" to "D9x", // hex value D9
            "{DA}" to "DAx", // hex value DA
            "{Dagger}" to "7Cx", // double dagger
            "{dagger}" to "7Cx", // dagger
            "{DB}" to "DBx", // hex value DB
            "{dblac}" to "EEx", // combining double acute
            "{dbldotb}" to "F3x", // combining double dot below
            "{dblunder}" to "F5x", // combining double underscore
            "{DC}" to "DCx", // hex value DC
            "{Dcaron}" to "E9x 44x", // latin large letter d with caron
            "{dcaron}" to "E9x 64x", // latin small letter d with caron
            "{Dcy}" to "44x", // cyrillic large letter de
            "{dcy}" to "64x", // cyrillic small letter de
            "{DD}" to "DDx", // hex value DD
            "{DE}" to "DEx", // hex value DE
            "{deg}" to "C0x", // degree sign
            "{DF}" to "DFx", // hex value DF
            "{diaer}" to "E8x", // combining diaeresis
            "{divide}" to "2Fx", // divide sign
            "{djecy}" to "B3x", // cyrillic small letter dje
            "{DJEcy}" to "A3x", // cyrillic large letter dje
            "{dollar}" to "24x", // dollar sign
            "{dot}" to "E7x", // combining dot above
            "{dotb}" to "F2x", // combining dot below
            "{Dstrok}" to "A3x", // latin large letter d with stroke
            "{dstrok}" to "B3x", // latin small letter d with stroke
            "{DZEcy}" to "44x 7Ax", // cyrillic large letter dze
            "{dzecy}" to "64x 7Ax", // cyrillic small letter dze
            "{DZHEcy}" to "44x E9x 7Ax", // cyrillic large letter dzhe
            "{dzhecy}" to "64x E9x 7Ax", // cyrillic small letter dzhe
            "{E}" to "45x", // latin large letter e
            "{e}" to "65x", // latin small letter e
            "{E0}" to "E0x", // combining hook above
            "{E1}" to "E1x", // combining grave
            "{E2}" to "E2x", // combining acute
            "{E3}" to "E3x", // combining circumflex
            "{E4}" to "E4x", // combining tilde
            "{E5}" to "E5x", // combining macron
            "{E6}" to "E6x", // combining breve
            "{E7}" to "E7x", // combining dot above
            "{E8}" to "E8x", // combining diaeresis
            "{E9}" to "E9x", // combining hacek
            "{EA}" to "EAx", // combining ring above
            "{ea}" to "eax", // combining ring above
            "{Eacute}" to "E2x 45x", // latin large letter e with acute
            "{eacute}" to "E2x 65x", // latin small letter e with acute
            "{EB}" to "EBx", // combining ligature left half
            "{EC}" to "ECx", // combining ligature right half
            "{Ecaron}" to "E9x 45x", // latin large letter e with caron
            "{ecaron}" to "E9x 65x", // latin small letter e with caron
            "{Ecirc}" to "E3x 45x", // latin large letter e with circumflex
            "{ecirc}" to "E3x 65x", // latin small letter e with circumflex
            "{Ecy}" to "E7x 44x", // cyrillic large letter reversed e
            "{ecy}" to "E7x 65x", // cyrillic small letter reversed e
            "{ED}" to "EDx", // combining comma above right
            "{EE}" to "EEx", // combining double acute
            "{EF}" to "EFx", // combining candrabindu
            "{Egrave}" to "E1x 45x", // latin large letter e with grave
            "{egrave}" to "E1x 65x", // latin small letter e with grave
            "{Ehookr}" to "F1x 45x", // latin large letter e with right hook (ogonek)
            "{ehookr}" to "F1x 65x", // latin small letter e with right hook (ogonek)
            "{Eogon}" to "F1x 45x", // latin large letter e with ogonek (right hook)
            "{eogon}" to "F1x 65x", // latin small letter e with ogonek (right hook)
            "{equals}" to "3Dx", // equals sign
            "{esc}" to "1Bx", // escape
            "{eth}" to "BAx", // latin small letter eth
            "{ETH}" to "A3x", // latin capital letter eth
            "{Euml}" to "E8x 45x", // latin large letter e with umlaut
            "{euml}" to "E8x 65x", // latin small letter e with umlaut
            "{excl}" to "21x", // exclamation point
            "{F}" to "46x", // latin large letter f
            "{f}" to "66x", // latin small letter f
            "{F0}" to "F0x", // combining cedilla
            "{F1}" to "F1x", // combining ogonek
            "{F2}" to "F2x", // combining dot below
            "{F3}" to "F3x", // combining double dot below
            "{F4}" to "F4x", // combining ring below
            "{F5}" to "F5x", // combining double underscore
            "{F6}" to "F6x", // combining underscore
            "{F7}" to "F7x", // combining comma below
            "{F8}" to "F8x", // combining right cedilla
            "{F9}" to "F9x", // combining breve below
            "{FA}" to "FAx", // combining double tilde left half
            "{FB}" to "FBx", // combining double tilde right half
            "{FC}" to "FCx", // hex value FC
            "{Fcy}" to "46x", // cyrillic large letter ef
            "{fcy}" to "66x", // cyrillic small letter ef
            "{FD}" to "FDx", // hex value FD
            "{FE}" to "FEx", // combining comma above
            "{FF}" to "FFx", // hex value FF
            "{flat}" to "A9x", // musical flat sign
            "{fnof}" to "66x", // curvy f (CP850)
            "{frac12}" to "31x 2Fx 32x", // fraction 1/2
            "{frac14}" to "31x 2Fx 34x", // fraction 1/4
            "{frac34}" to "33x 2Fx 34x", // fraction 3/4
            "{G}" to "47x", // latin large letter g
            "{g}" to "67x", // latin small letter g
            "{Gcy}" to "47x", // cyrillic large letter ge
            "{gcy}" to "67x", // cyrillic small letter ge
            "{GEcy}" to "47x", // cyrillic large letter ge
            "{gecy}" to "67x", // cyrillic small letter ge
            "{ggr}" to "67x", // greek small letter gamma
            "{GHcy}" to "47x", // ukrainian/belorussian large letter ghe
            "{ghcy}" to "67x", // ukrainian/belorussian small letter ghe
            "{GJEcy}" to "E2x 47x", // cyrillic large letter gje
            "{gjecy}" to "E2x 67x", // cyrillic small letter gje
            "{grave}" to "E1x", // combining grave
            "{greek}" to "67x", // begin greek script
            "{gs}" to "1Dx", // group separator (end of record)
            "{gt}" to "3Ex", // greater than
            "{H}" to "48x", // latin large letter h
            "{h}" to "68x", // latin small letter h
            "{HARDcy}" to "B7x", // cyrillic large letter hardsign
            "{hardcy}" to "B7x", // cyrillic small letter hardsign
            "{hardsign}" to "B7x", // modifier letter hard sign
            "{Hcy}" to "48x", // cyrillic large letter he
            "{hcy}" to "68x", // cyrillic small letter he
            "{hebrew}" to "28x 32x", // begin hebrew script
            "{hellip}" to "2Ex 2Ex 2Ex", // ellipsis
            "{hooka}" to "E0x", // combining hook above
            "{hookl}" to "F7x", // combining hook left (comma below)
            "{hookr}" to "F1x", // combining hook right (ogonek)
            "{hyphen}" to "2Dx", // hyphen (minus)
            "{I}" to "49x", // latin large letter i
            "{i}" to "69x", // latin small letter i
            "{Iacute}" to "E2x 49x", // latin large letter i with acute
            "{iacute}" to "E2x 69x", // latin small letter i with acute
            "{Icaron}" to "E9x 49x", // latin large letter i with caron
            "{icaron}" to "E9x 69x", // latin small letter i with caron
            "{Icirc}" to "E3x 49x", // latin large letter i with circumflex
            "{icirc}" to "E3x 69x", // latin small letter i with circumflex
            "{Icy}" to "49x", // cyrillic large letter ii
            "{icy}" to "69x", // cyrillic small letter ii
            "{Idot}" to "E7x 49x", // latin small letter i with dot
            "{IEcy}" to "EBx 49x ECx 45x", // cyrillic large letter ie
            "{iecy}" to "EBx 69x ECx 65x", // cyrillic large letter ie
            "{iexcl}" to "C6x", // inverted exclamation mark
            "{Igrave}" to "E1x 49x", // latin large letter i with grave
            "{igrave}" to "E1x 69x", // latin small letter i with grave
            "{IJlig}" to "49x 4Ax", // latin large letter ij
            "{ijlig}" to "69x 6Ax", // latin small letter ij
            "{inodot}" to "B8x", // latin small letter dotless i
            "{IOcy}" to "EBx 49x ECx 4Fx", // cyrillic large letter io
            "{iocy}" to "EBx 69x ECx 6Fx", // cyrillic small letter io
            "{iquest}" to "C5x", // inverted question mark
            "{Iuml}" to "E8x 49x", // latin large letter i with umlaut
            "{iuml}" to "E8x 69x", // latin small letter i with umlaut
            "{Iumlcy}" to "E8x 49x", // cyrillic large letter i with umlaut
            "{iumlcy}" to "E8x 69x", // cyrillic small letter i with umlaut
            "{IYcy}" to "59x", // cyrillic large letter ukrainian y
            "{iycy}" to "79x", // cyrillic small small letter ukrainian y
            "{J}" to "4Ax", // latin large letter j
            "{j}" to "6Ax", // latin small letter j
            "{Jcy}" to "E6x 49x", // cyrillic large letter short ii
            "{jcy}" to "E6x 69x", // cyrillic small letter short ii
            "{JEcy}" to "4Ax", // cyrillic large letter je
            "{jecy}" to "6Ax", // cyrillic small letter je
            "{JIcy}" to "E8x 49x", // cyrillic large letter ji
            "{jicy}" to "E8x 69x", // cyrillic small letter ji
            "{joiner}" to "8Dx", // zero width joiner
            "{K}" to "4Bx", // latin large letter k
            "{k}" to "6Bx", // latin small letter k
            "{Kcy}" to "4Bx", // cyrillic large letter ka
            "{kcy}" to "6Bx", // cyrillic small letter ka
            "{KHcy}" to "4Bx 68x", // cyrillic large letter kha
            "{khcy}" to "6Bx 68x", // cyrillic small letter kha
            "{KJEcy}" to "E2x 4Bx", // cyrillic large letter kje
            "{kjecy}" to "E2x 6Bx", // cyrillic small letter kje
            "{L}" to "4Cx", // latin large letter l
            "{l}" to "6Cx", // latin small letter l
            "{Lacute}" to "E2x 4Cx", // latin large letter l with acute
            "{lacute}" to "E2x 6Cx", // latin small letter l with acute
            "{laquo}" to "22x", // left-pointing double angle quote mark
            "{latin}" to "28x 42x", // begin latin script
            "{lcub}" to "7Bx", // opening curly brace
            "{Lcy}" to "4Cx", // cyrillic large letter el
            "{lcy}" to "6Cx", // cyrillic small letter el
            "{ldbltil}" to "FAx", // combining double tilde left half
            "{ldquo}" to "22x", // left double quote mark
            "{LJEcy}" to "4Cx 6Ax", // cyrillic large letter lje
            "{ljecy}" to "6Cx 6Ax", // cyrillic small letter lje
            "{llig}" to "EBx", // combining ligature left half
            "{lpar}" to "28x", // left parenthesis
            "{lsqb}" to "5Bx", // left bracket
            "{lsquo}" to "27x", // left single quotation mark
            "{lsquor}" to "27x", // rising single quotation left (low)
            "{Lstrok}" to "A1x", // latin large letter l with stroke
            "{lstrok}" to "B1x", // latin small letter l with stroke
            "{lt}" to "3Cx", // less than
            "{M}" to "4Dx", // latin large letter m
            "{m}" to "6Dx", // latin small letter m
            "{macr}" to "E5x", // combining macron
            "{Mcy}" to "4Dx", // cyrillic large letter em
            "{mcy}" to "6Dx", // cyrillic small letter em
            "{mdash}" to "2Dx 2Dx", // m dash
            "{middot}" to "A8x", // middle dot
            "{mllhring}" to "B0x", // modifier letter left half ring (ayn)
            "{mlprime}" to "A7x", // modifier letter prime (soft sign)
            "{mlPrime}" to "B7x", // modifier letter double prime (hard sign)
            "{mlrhring}" to "AEx", // modifier letter right half ring (alif)
            "{N}" to "4Ex", // latin large letter n
            "{n}" to "6Ex", // latin small letter n
            "{Nacute}" to "E2x 4Ex", // latin large letter n with acute
            "{nacute}" to "E2x 6Ex", // latin small letter n with acute
            "{Ncaron}" to "E9x 4Ex", // latin large letter n with caron
            "{ncaron}" to "E9x 6Ex", // latin small letter n with caron
            "{Ncy}" to "4Ex", // cyrillic large letter en
            "{ncy}" to "6Ex", // cyrillic small letter en
            "{ndash}" to "2Dx 2Dx", // m dash
            "{NJEcy}" to "4Ex 6Ax", // cyrillic large letter nj
            "{njecy}" to "6Ex 6Ax", // cyrillic small letter nj
            "{No}" to "4Ex 6Fx 2Ex", // cyrillic abbr. for "nomer"
            "{nonjoin}" to "8Ex", // zero width non-joiner
            "{Ntilde}" to "E4x 4Ex", // latin large letter n with tilde
            "{ntilde}" to "E4x 6Ex", // latin small letter n with tilde
            "{num}" to "23x", // number sign
            "{O}" to "4Fx", // latin large letter o
            "{o}" to "6Fx", // latin small letter o
            "{Oacute}" to "E2x 4Fx", // latin large letter o with acute
            "{oacute}" to "E2x 6Fx", // latin small letter o with acute
            "{Ocirc}" to "E3x 4Fx", // latin large letter o with circ
            "{ocirc}" to "E3x 6Fx", // latin small letter o with circ
            "{Ocy}" to "4Fx", // cyrillic large letter o
            "{ocy}" to "6Fx", // cyrillic small letter o
            "{Odblac}" to "EEx 4Fx", // latin large letter o double acute
            "{odblac}" to "EEx 6Fx", // latin small letter o double acute
            "{OElig}" to "A6x", // latin large letter OE
            "{oelig}" to "B6x", // latin small letter oe
            "{ogon}" to "F1x", // combining ogonek (hook right)
            "{Ograve}" to "E1x 4Fx", // latin large letter o with grave
            "{ograve}" to "E1x 6Fx", // latin small letter o with grave
            "{Ohorn}" to "ACx", // latin large letter o with horn
            "{ohorn}" to "BCx", // latin small letter o with horn
            "{ordf}" to "61x", // feminine ordinal indicator
            "{ordm}" to "6Fx", // masculine ordinal indicator
            "{Ostrok}" to "A2x", // latin large letter o with stroke
            "{ostrok}" to "B2x", // latin small letter o with stroke
            "{osuper}" to "6Fx", // latin small letter superscript o
            "{Otilde}" to "E4x 4Fx", // latin large letter o with tilde
            "{otilde}" to "E4x 6Fx", // latin small letter o with tilde
            "{Ouml}" to "E8x 4Fx", // latin large letter o with uml
            "{ouml}" to "E8x 6Fx", // latin small letter o with uml
            "{P}" to "50x", // latin large letter p
            "{p}" to "70x", // latin small letter p
            "{para}" to "7Cx", // pilcrow (paragraph)
            "{Pcy}" to "50x", // cyrillic large letter pe
            "{pcy}" to "70x", // cyrillic small letter pe
            "{percnt}" to "25x", // percent sign
            "{period}" to "2Ex", // period (decimal point)
            "{phono}" to "C2x", // sound recording copyright
            "{pipe}" to "7Cx", // pipe
            "{plus}" to "2Bx", // plus
            "{plusmn}" to "ABx", // plus-minus sign
            "{pound}" to "B9x", // pound sign
            "{Q}" to "51x", // latin large letter q
            "{q}" to "71x", // latin small letter q
            "{quest}" to "3Fx", // question mark
            "{quot}" to "22x", // quotation mark
            "{R}" to "52x", // latin large letter r
            "{r}" to "72x", // latin small letter r
            "{Racute}" to "E2x 52x", // latin large letter r with acute
            "{racute}" to "E2x 72x", // latin small letter r with acute
            "{raquo}" to "22x", // right-pointing double angle quotation mark
            "{Rcaron}" to "E9x 52x", // latin large letter r with caron
            "{rcaron}" to "E9x 72x", // latin small letter r with caron
            "{rcedil}" to "F8x", // combining right cedilla
            "{rcommaa}" to "EDx", // combining comma above right
            "{rcub}" to "7Dx", // closing curly brace
            "{Rcy}" to "52x", // cyrillic large letter er
            "{rcy}" to "72x", // cyrillic small letter er
            "{rdbltil}" to "FBx", // combining double tilde right half
            "{rdquofh}" to "22x", // falling double quotation right (high)
            "{rdquor}" to "22x", // rising double quotation right (high)
            "{reg}" to "AAx", // registered sign
            "{ring}" to "EAx", // combining ring above
            "{ringb}" to "F4x", // combining ring below
            "{rlig}" to "ECx", // combining ligature right half
            "{rpar}" to "29x", // right parenthesis
            "{rs}" to "1Ex", // record separator (end of field)
            "{rsqb}" to "5Dx", // right bracket
            "{rsquo}" to "27x", // right single quotation mark
            "{rsquor}" to "27x", // rising single quotation right (high)
            "{S}" to "53x", // latin large letter s
            "{s}" to "73x", // latin small letter s
            "{Sacute}" to "E2x 53x", // latin capital s with acute
            "{sacute}" to "E2x 73x", // latin small s with acute
            "{Scommab}" to "F7x 53x", // latin large letter s with comma below
            "{scommab}" to "F7x 73x", // latin small letter s with comma below
            "{scriptl}" to "C1x", // latin small letter script l
            "{Scy}" to "53x", // cyrillic large letter es
            "{scy}" to "73x", // cyrillic small letter es
            "{sect}" to "7Cx", // section sign
            "{semi}" to "3Bx", // semicolon
            "{sharp}" to "C4x", // sharp
            "{SHCHcy}" to "53x 68x 63x 68x", // cyrillic large letter shcha
            "{shchcy}" to "73x 68x 63x 68x", // cyrillic small letter shcha
            "{SHcy}" to "53x 68x", // cyrillic large letter sha
            "{shcy}" to "73x 68x", // cyrillic small letter sha
            "{shy}" to "2Dx", // soft hyphen (CP850)
            "{SOFTcy}" to "A7x", // cyrillic large letter softsign
            "{softcy}" to "A7x", // cyrillic smalll letter softsign
            "{softsign}" to "A7x", // modifier letter soft sign
            "{sol}" to "2Fx", // slash (solidus)
            "{space}" to "20x", // space (blank)
            "{spcirc}" to "5Ex", // spacing circumflex
            "{spgrave}" to "60x", // spacing grave
            "{sptilde}" to "7Ex", // spacing tilde
            "{spundscr}" to "5Fx", // spacing underscore
            "{squf}" to "7Cx", // fill character
            "{sub}" to "62x", // begin subscript
            "{sup1}" to "1Bx 70x 31x 1Bx 73x", // 027d 112d 049d 027d 115d
            "{sup2}" to "1Bx 70x 32x 1Bx 73x", // 027d 112d 050d 027d 115d
            "{sup3}" to "1Bx 70x 33x 1Bx 73x", // 027d 112d 051d 027d 115d
            "{super}" to "70x", // begin superscript
            "{szlig}" to "73x 73x", // latin small letter sharp s (german)
            "{T}" to "54x", // latin large letter t
            "{t}" to "74x", // latin small letter t
            "{Tcaron}" to "E9x 54x", // latin large letter t with caron
            "{tcaron}" to "E9x 74x", // latin small letter t with caron
            "{Tcommab}" to "F7x 54x", // latin large letter t with comma below (hook left)
            "{tcommab}" to "F7x 74x", // latin small letter t with comma below (hook left)
            "{Tcy}" to "54x", // cyrillic large letter te
            "{tcy}" to "74x", // cyrillic small letter te
            "{THORN}" to "A4x", // latin large letter thorn (icelandic)
            "{thorn}" to "B4x", // latin small letter thorn (icelandic)
            "{tilde}" to "E4x", // combining tilde
            "{times}" to "78x", // times sign
            "{trade}" to "28x 54x 6Dx 29x", // trade mark sign
            "{TScy}" to "EBx 54x ECx 53x", // cyrillic large letter tse
            "{tscy}" to "EBx 74x ECx 73x", // cyrillic small letter tse
            "{TSHEcy}" to "E2x 43x", // latin large letter tshe
            "{tshecy}" to "E2x 63x", // latin small letter tshe
            "{U}" to "55x", // latin large letter u
            "{u}" to "75x", // latin small letter u
            "{Uacute}" to "E2x 55x", // latin large letter u with acute
            "{uacute}" to "E2x 75x", // latin small letter u with acute
            "{Ubrevecy}" to "E6x 55x", // cyrillic large letter u with breve
            "{ubrevecy}" to "E6x 75x", // cyrillic small letter u with breve
            "{Ucirc}" to "E3x 55x", // latin large letter u with circ
            "{ucirc}" to "E3x 75x", // latin small letter u with circ
            "{Ucy}" to "55x", // cyrillic large letter u
            "{ucy}" to "75x", // cyrillic small letter u
            "{Udblac}" to "EEx 55x", // latin large letter u with double acute
            "{udblac}" to "EEx 75x", // latin small letter u with double acute
            "{Ugrave}" to "E1x 55x", // latin large letter u with grave
            "{ugrave}" to "E1x 75x", // latin small letter u with grave
            "{Uhorn}" to "ADx", // latin large letter u with horn
            "{uhorn}" to "BDx", // latin small letter u with horn
            "{uml}" to "E8x", // combining umlaut
            "{under}" to "F6x", // combining underscore
            "{Uring}" to "EAx 55x", // latin large letter u with ring
            "{uring}" to "EAx 75x", // latin small letter u with ring
            "{us}" to "1Fx", // unit separator (subfield delimiter)
            "{Uuml}" to "E8x 55x", // latin large letter u with uml
            "{uuml}" to "E8x 75x", // latin small letter u with uml
            "{V}" to "56x", // latin large letter v
            "{v}" to "76x", // latin small letter v
            "{Vcy}" to "56x", // cyrillic large letter ve
            "{vcy}" to "76x", // cyrillic small letter ve
            "{verbar}" to "7Cx", // vertical bar (fill character)
            "{vlineb}" to "F2x", // combining vertical line below
            "{W}" to "57x", // latin large letter w
            "{w}" to "77x", // latin small letter w
            "{X}" to "58x", // latin large letter x
            "{x}" to "78x", // latin small letter x
            "{Y}" to "59x", // latin large letter y
            "{y}" to "79x", // latin small letter y
            "{Yacute}" to "E2x 59x", // latin large letter y
            "{yacute}" to "E2x 79x", // latin small letter y
            "{YAcy}" to "EBx 49x ECx 41x", // cyrillic large letter ia
            "{yacy}" to "EBx 69x ECx 61x", // cyrillic small letter ia
            "{Ycy}" to "59x", // cyrillic large letter yeri
            "{ycy}" to "79x", // cyrillic small letter yeri
            "{YEcy}" to "45x", // cyrillic large letter ye
            "{yecy}" to "65x", // cyrillic small letter ye
            "{yen}" to "59x", // yen (CP850)
            "{YIcy}" to "49x", // cyrillic large letter yi
            "{yicy}" to "69x", // cyrillic small letter yi
            "{YUcy}" to "EBx 49x ECx 55x", // cyrillic large letter iu
            "{yucy}" to "EBx 69x ECx 75x", // cyrillic small letter iu
            "{Z}" to "5Ax", // latin large letter z
            "{z}" to "7Ax", // latin small letter z
            "{Zacute}" to "E2x 5Ax", // latin large letter z with acute
            "{zacute}" to "E2x 7Ax", // latin small letter z with acute
            "{Zcy}" to "5Ax", // cyrillic large letter ze
            "{zcy}" to "7Ax", // cyrillic small letter ze
            "{Zdot}" to "E7x 5Ax", // latin large letter z with dot above
            "{zdot}" to "E7x 7Ax", // latin small letter z with dot above
            "{ZHcy}" to "5Ax 68x", // cyrillic large letter zhe
            "{zhcy}" to "7Ax 68x", // cyrillic small letter zhe
            "{ZHuacy}" to "EBx 5Ax ECx 68x", // ukrainian large letter zhe
            "{zhuacy}" to "EBx 7Ax ECx 68x", // ukrainian small letter zhe
        )
        val charToNibble: (Char) -> Int = {
            when (it) {
                in '0'..'9' -> it - '0'
                in 'A'..'F' -> it - 'A' + 10
                in 'a'..'f' -> it - 'a' + 10
                else -> 0
            }
        }
        val translate: (String) -> String = {
            var str = ""
            for (i in it.indices step 4) {
                str += (charToNibble(it[i]) * 16 + charToNibble(it[i + 1])).toChar()
            }
            str
        }
        mrk8Table = tables.mapValues { (_, value) ->
            translate(value)
        }
    }

    fun toMrk8(dataField: String): String {
        return dataField.toCharArray().joinToString("") {
            when (it) {
                '$' -> "{dollar}"
                '{' -> "{lcub}"
                '}' -> "{rcub}"
                else -> it.toString()
            }
        }
    }

    fun fromMrk8(dataField: String): String {
        if (!dataField.contains('{')) {
            return dataField
        }
        var str = ""
        val len = dataField.length
        var i = 0
        while (i < len) {
            val j = dataField.indexOf('{', i)
            if (j == -1) {
                str += dataField.substring(i)
                break
            }
            if (i < j) {
                str += dataField.substring(i, j)
            }
            val k = dataField.indexOf('}', j)
            val lookUpValue = dataField.substring(j, k + 1)
            str += lookup(lookUpValue)
            i = k + 1
        }
        return str
    }

    private fun lookup(needle: String): String {
        return mrk8Table[needle] ?: needle
    }
}
