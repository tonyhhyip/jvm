package me.tonyhhyip.bibliography.marc.serialize.mrk8.spi

import me.tonyhhyip.bibliography.marc.serialize.mrk8.Mrk8MarcReader
import me.tonyhhyip.bibliography.marc.serialize.mrk8.Mrk8MarcWriter
import me.tonyhhyip.bibliography.marc.serialize.spi.SerializerProvider

class Mrk8SerializerProvider : SerializerProvider {
    override fun reader() = Mrk8MarcReader
    override fun writer() = Mrk8MarcWriter
}
