package me.tonyhhyip.bibliography.marc.serialize.mrk8

import me.tonyhhyip.bibliography.marc.core.Record
import me.tonyhhyip.bibliography.marc.serialize.MarcWriter
import java.io.OutputStream
import java.io.OutputStreamWriter
import java.io.PrintWriter
import java.nio.charset.StandardCharsets

object Mrk8MarcWriter : MarcWriter {
    override val fileExtension = "mrk8"
    override val format = "mrk8"

    override fun write(output: OutputStream, records: Collection<Record>) {
        PrintWriter(OutputStreamWriter(output, StandardCharsets.UTF_8)).use { writer ->
            records.forEach { record ->
                var str = "=LDR  ${record.leader}${System.lineSeparator()}"
                record.controlFields.forEach { field ->
                    str += "=${field.tag}  ${field.data.replace(' ', '\\')}${System.lineSeparator()}"
                }
                record.dataFields.forEach { field ->
                    str += "=${field.tag}  "
                    str += if (field.indicator1 == ' ') "\\" else field.indicator1
                    str += if (field.indicator2 == ' ') "\\" else field.indicator2
                    field.getSubfields().forEach {
                        str += "\$${it.code}${Mrk8TranslationTable.toMrk8(it.data)}"
                    }
                    str += System.lineSeparator()
                }
                str += System.lineSeparator()
                writer.append(str)
                writer.flush()
            }
        }
    }
}
