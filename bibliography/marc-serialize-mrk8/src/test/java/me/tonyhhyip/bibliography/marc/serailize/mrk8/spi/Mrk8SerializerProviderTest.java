package me.tonyhhyip.bibliography.marc.serailize.mrk8.spi;

import me.tonyhhyip.bibliography.marc.serialize.mrk8.Mrk8MarcReader;
import me.tonyhhyip.bibliography.marc.serialize.mrk8.Mrk8MarcWriter;
import me.tonyhhyip.bibliography.marc.serialize.mrk8.spi.Mrk8SerializerProvider;
import me.tonyhhyip.bibliography.marc.serialize.spi.SerializerProvider;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class Mrk8SerializerProviderTest {
  @Test
  void testProvider() {
    SerializerProvider provider = new Mrk8SerializerProvider();
    Assertions.assertEquals(Mrk8MarcReader.INSTANCE, provider.reader());
    Assertions.assertEquals(Mrk8MarcWriter.INSTANCE, provider.writer());
  }
}
