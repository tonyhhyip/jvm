package me.tonyhhyip.bibliography.marc.serailize.mrk8;

import java.io.InputStream;
import me.tonyhhyip.bibliography.marc.core.Record;
import me.tonyhhyip.bibliography.marc.serialize.MarcReader;
import me.tonyhhyip.bibliography.marc.serialize.mrk8.Mrk8MarcReader;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class Mrk8MarcReaderTest {
  @Test
  void testReading() {
    InputStream input = this.getClass().getResourceAsStream("/test.mrk8");
    MarcReader reader = Mrk8MarcReader.INSTANCE;
    Assertions.assertEquals("mrk8", reader.getFileExtension());
    Iterable<Record> iterable = reader.parse(input);
    int count = 0;
    for (Record record : iterable) {
      count += 1;
      if (count == 1) {
        Assertions.assertEquals("01201nam a2200253 a 4500", record.getLeader().toString());
      }
    }
    Assertions.assertEquals(8, count);
  }
}
