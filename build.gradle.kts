plugins {
    val kotlinVersion = "1.5.20"
    idea

    kotlin("jvm") version kotlinVersion apply false
    kotlin("plugin.spring") version kotlinVersion apply false
    kotlin("plugin.jpa") version kotlinVersion apply false

    id("com.diffplug.spotless") version "5.14.0"
    id("org.sonarqube") version "3.3"
    id("org.owasp.dependencycheck") version "6.2.0"

    id("org.springframework.boot") version "2.5.2" apply false
    id("io.spring.dependency-management") version "1.0.10.RELEASE" apply false
}

val ktlintVersion by extra { "0.41.0" }
val googleJavaVersion by extra { "1.9" }
val jacocoVersion by extra { "0.8.7" }

allprojects {
    group = "me.tonyhhyip.jvm"
    repositories {
        mavenLocal()
        mavenCentral()
        maven(url = "https://gitlab.com/api/v4/projects/22919357/packages/maven")
    }

    apply(plugin = "com.diffplug.spotless")
    spotless {
        kotlinGradle {
            ktlint("0.41.0")
        }
    }

    apply(plugin = "org.sonarqube")
    sonarqube {
        properties {
            property("sonar.sourceEncoding", "UTF-8")
            property("sonar.organization", "tonyhhyip")
        }
    }
}

subprojects {
    tasks.withType<Test> {
        useJUnitPlatform()
    }

    tasks.withType<org.jetbrains.kotlin.gradle.tasks.KotlinCompile> {
        kotlinOptions {
            freeCompilerArgs = listOf("-Xjsr305=strict")
            jvmTarget = "11"
        }
    }
}
